/*
 * usb_tirtos.h
 *
 *  Created on: Mar 11, 2019
 *      Author: LTran
 */

#ifndef USB_TIRTOS_H_
#define USB_TIRTOS_H_

void *usb_transmitFxn(void *arg0);
void *usb_receiveFxn(void *arg0);

void *usbThread(void);




#endif /* USB_TIRTOS_H_ */
