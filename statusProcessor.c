/*
 * statusProcessor.c
 *
 * This module contains the functions that gather the status
 * information and create the JSON messages that are sent to the
 * server
 *
 *  Created on: Jan 3, 2019
 *      Author: tjc2c
 */


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#include "json/tiny-json.h"
#include "json/json-maker.h"

#include "piccolo.h"
#include "commandProcessor.h"
#include "statusProcessor.h"

char *jsonCommonStatusParams(char* dest, char const* name, statusMsg_t const* status) {

    dest = json_int( dest, "code", status->code );
    dest = json_int( dest, "format", status->format );
    dest = json_str( dest, "deviceId", status->deviceId );
    dest = json_str( dest, "mac", status->MAC );
    dest = json_str( dest, "sensorId", status->sensorId );
    dest = json_str( dest, "siteId", status->siteId );
    dest = json_str( dest, "dateTime", status->dateTime );
    dest = json_str( dest, "timeOffset", status->timeOffset );

    return dest;

}
/**
 * Builds the registration message json string
 *
 *  @param dest JSON string buffer
 *  @param name JSON property name
 *  @param status Pointer to status data structure
 *
 *  @return JSON status message string
 */
char* jsonRegistration( char* dest, char const* name, statusMsg_t const* status ) {
    dest = json_objOpen( dest, name );
    dest = jsonCommonStatusParams(dest,name,status);
    dest = json_int( dest, "ambientTemp", status->ambientTemp );
    dest = json_int( dest, "unitTemp1", status->unitTemp1 );
    dest = json_int( dest, "unitTemp2", status->unitTemp2 );
    dest = json_double( dest, "panelVoltage", status->panelVoltage );
    dest = json_double( dest, "panelCurrent", status->panelCurrent );
    dest = json_double( dest, "panelPower", status->panelPower );
    dest = json_int( dest, "pulseVoltage", status->pulseVoltage );
    dest = json_int( dest, "pulseWidth", status->pulseWidth );
    dest = json_double( dest, "pri", status->pri );
    dest = json_double( dest, "phaseDelay", status->phaseDelay );
    dest = json_int( dest, "pulsePlan", status->pulsePlan );
    dest = json_bool( dest, "alert", status->alert );
    dest = json_int( dest, "iotMessageId", status->messageId );
    dest = json_objClose( dest );
    return dest;
}

/**
 * Builds the ongoing status message json string
 *
 *  @param dest JSON string buffer
 *  @param name JSON property name
 *  @param status Pointer to status data structure
 *
 *  @return JSON status message string
 */
char* jsonStatus( char* dest, char const* name, statusMsg_t const* status ) {
    dest = json_objOpen( dest, name );              // --> "statusLog":{\0
    dest = jsonCommonStatusParams(dest,name,status);
    dest = json_int( dest, "ambientTemp", status->ambientTemp );
    dest = json_int( dest, "unitTemp1", status->unitTemp1 );
    dest = json_int( dest, "unitTemp2", status->unitTemp2 );
    dest = json_str( dest, "dateTime", status->dateTime );
    dest = json_str( dest, "timeOffset", status->timeOffset );
    dest = json_str( dest, "pulseMode", currentPulseMode );

    dest = json_bool( dest, "alert", status->alert );
    dest = json_int( dest, "iotMessageId", status->messageId );
    dest = json_objClose( dest );
    return dest;
}

/**
 * Test function generates random temperature values in the
 * 100 - 200 range
 */
int getRandTemp() {
    double scale = (double)rand()/32768.0;
    int retVal = 100 + 100*scale;
    return retVal;
}

/**
 * Retrieves the registration data, populates
 * the status data structure and creates the status message
 * JSON string to send to the server
 *
 *      @param rInfo    buffer containing the registration JSON data
 */
void getRegistrationData(char *rInfo) {

    time_t t;
    struct tm *ltm;
    char curTime[20];
    statusMsg_t status;

    t = time(NULL);
    ltm = localtime(&t);
    //YYYY-DD-MM HH:MM:SS
    strftime(curTime,20,"%F %T",ltm);


    Display_printf(display, 0, 0, "getRegistrationData: Time = %s", curTime);


    Display_printf(display, 0, 0, "getRegistrationData: Rand = %d", getRandTemp());


    // Registration status code = 1
    status.code = 0;
    status.format = 0;
    // Device specific
    strcpy(status.deviceId,deviceConfigParams.deviceId);
    strcpy(status.MAC,deviceConfigParams.macAddress);
    strcpy(status.siteId,deviceConfigParams.siteId);
    strcpy(status.sensorId,deviceConfigParams.sensorId);
    strcpy(status.dateTime,curTime);
    strcpy(status.timeOffset,"-07:00");
    status.ambientTemp = getRandTemp();
    status.unitTemp1 = getRandTemp();
    status.unitTemp2 = getRandTemp();
    getPanelMeasurements();
    status.panelVoltage = panelMeasurements.panelVoltage;
    status.panelCurrent = panelMeasurements.panelCurrent;
    status.panelPower = panelMeasurements.panelPower;
    status.pulseVoltage = 0;
    status.pulseWidth = 0;
    status.pri = 0.0;
    status.phaseDelay = 0.0;
    status.alert = false;
    status.messageId = deviceConfigParams.messageCnt;

    rInfo = json_objOpen(rInfo,NULL);

    rInfo = jsonRegistration(rInfo,"statusLog",&status);

    rInfo = json_objClose(rInfo);
    json_end(rInfo);

}

/**
 * Retrieves the ongoing status data, populates
 * the status data structure and creates the status message
 * JSON string to send to the server
 *
 *      @param sInfo    buffer containing the registration JSON data
 */
void getStatusData(char *sInfo) {


    time_t t;
    struct tm *ltm;
    char curTime[20];
    statusMsg_t status;

    t = time(NULL);
    ltm = localtime(&t);
    //YYYY-DD-MMTHH:MM:SS
    strftime(curTime,20,"%F %T",ltm);

    Display_printf(display, 0, 0, "getStatusData: Time = %s", curTime);

    deviceConfigParams.messageCnt++;

    Display_printf(display, 0, 0, "getRegistrationData: Rand = %d", getRandTemp());

    // Ongoing status code = 1
    status.code = 1;
    status.format = 0;
    // Device specific
    strcpy(status.deviceId,deviceConfigParams.deviceId);
    strcpy(status.MAC,deviceConfigParams.macAddress);
    strcpy(status.siteId,deviceConfigParams.siteId);
    strcpy(status.sensorId,deviceConfigParams.sensorId);
    strcpy(status.dateTime,curTime);
    strcpy(status.timeOffset,"-07:00");
    status.ambientTemp = getRandTemp();
    status.unitTemp1 = getRandTemp();
    status.unitTemp2 = getRandTemp();
    status.alert = false;
    status.messageId = deviceConfigParams.messageCnt;

    sInfo = json_objOpen(sInfo,NULL);

    sInfo = jsonStatus(sInfo,"statusLog",&status);

    sInfo = json_objClose(sInfo);
    json_end(sInfo);

}
