/*
 * parseCmd.h
 *
 *  Created on: Mar 4, 2019
 *      Author: LTran
 */



#ifndef PARSECMD_H_
#define PARSECMD_H_



typedef enum cmd_token_t        /* Command token */
{
    UNKNOWN,            /* 00:                          */
    COMMANDS,
    PULSER_CONTROL,
    WIFI_CONTROL,

} cmd_token;

#define NARG 3                      /* max. number of arguments (excl. optional) */

typedef struct cmd_struct_t         /* command structure */
{
    long            acount;         /* active counter: neg=not active, 0=dead, pos=alive */
    unsigned long   arg[NARG];      /* numeric arguments */
    cmd_token       cmd;            /* command */

} cmd_struct;


/*-------------------------------------------------------------------*/
/*                                                                   */
/* Function Name: parse_cmd                                          */
/*                                                                   */
/* Funct. Description:                                               */
/*    The function parses the received command string. If the        */
/*    parsing is failed, a error message will be sent back,          */
/*    otherwise the parsed command will be stacked in the            */
/*    command buffer.                                                */
/*    The function returns TRUE if the parsing passed, otherwise     */
/*    FALSE.                                                         */
/*                                                                   */
/*-------------------------------------------------------------------*/
void perform_cmd(cmd_token cmd_t, char *cmds);
//void perform_cmd(cmd_token cmd_t);
cmd_token scanner( char *cmds );
#endif

