
/*
 *  ======== main_tirtos.c ========
 */
/* Standard C library header file */
#include <stdbool.h>

/* POSIX Header files */
#include <pthread.h>

/* RTOS header files */
#include <ti/sysbios/BIOS.h>

/* Example/Board Header files */
#include "Board_usb.h"

#include "USBCDCD.h"
#include "console.h"
#include "usb_tirtos.h"


#define THREADSTACKSIZE   768



char *text = "TI-RTOS controls USB.\r\n" \
             "Enter Commands:\r\n" \
             "Pulser Commands: prx..x, pwx..x, pdx..x, pav..v:\r\n" \
             "  pr= pulser repetition rate, x..x = decimal number in p seconds\r\n" \
             "  pw= pulser width, x..x = decimal number in p seconds\r\n" \
             "  pd= pulser delay, x..x = decimal number in p seconds\r\n" \
             "  pa= pulser amplitude, v..v = decimal number in volts\r\n" \
             ">";

/*
 * The following (weak) function definition is needed in applications
 * that do *not* use the NDK TCP/IP stack:
 */
#if defined(__IAR_SYSTEMS_ICC__)
__weak void NDK_hookInit(int32_t id) {}
#elif defined(__GNUC__) && !defined(__ti__)
void __attribute__((weak)) NDK_hookInit(int32_t id) {}
#else
#pragma WEAK (NDK_hookInit)
void NDK_hookInit(int32_t id) {}
#endif

//int main(void)
//void *usbThread(void *arg0)
void *usbThread(void)
{
    pthread_t         thread;
    pthread_attr_t    attrs;
    struct sched_param  priParam;
    int                 retc;
 
    MSP_EXP432E401Y_initUSB(MSP_EXP432E401Y_USBDEVICE);

    USBCDCD_init(Board_USBDEVICE);
      /* Set priority and stack size attributes */
    pthread_attr_init(&attrs);
    priParam.sched_priority = 1;

    retc = pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);
    if (retc != 0) {
        /* pthread_attr_setdetachstate() failed */
        while (1);
    }

    pthread_attr_setschedparam(&attrs, &priParam);

    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }

    retc = pthread_create(&thread, &attrs, usb_transmitFxn, (void *)text);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }


   // priParam.sched_priority = 2;
    priParam.sched_priority = 1;

    pthread_attr_setschedparam(&attrs, &priParam);

 //   extern int pthread_create(pthread_t *newthread, const pthread_attr_t *attr,
 //               void *(*startroutine)(void *), void *arg);

   retc = pthread_create(&thread, &attrs, usb_receiveFxn, NULL);

    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    return (0);
}
