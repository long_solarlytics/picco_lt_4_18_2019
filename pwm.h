/*
 * pwm.h
 *
 *  Created on: Feb 28, 2019
 *      Author: LTran
 */

#ifndef PWM_H_
#define PWM_H_

void * pwmThread(void *arg0);
void redoPWM_Init(void);
void PulserChangePeriod(uint32_t period); //period = ns
void pwm_init(void);

#endif /* PWM_H_ */
