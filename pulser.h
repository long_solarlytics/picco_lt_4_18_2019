/*
 * pulser.h
 *
 *  Created on: Mar 7, 2019
 *      Author: LTran
 */

#ifndef PULSER_H_
#define PULSER_H_

#include <stdint.h>
typedef enum {
    PULSER_CTL_REPEATRATE,    //pr
    PULSER_CTL_WIDTH,           //pw
    PULSER_CTL_PHASEDELAY,           //pd
    PULSER_CTL_VOLTAGE,
    PULSER_CTL_START,          // ps
    PULSER_CTL_END             // pe


}pulserControlMode_enum;

void PulserControl(pulserControlMode_enum pulserControlMode, uint32_t value);

#endif /* PULSER_H_ */
