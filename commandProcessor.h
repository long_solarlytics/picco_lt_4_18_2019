/*
 * commandProcessor.h
 *
 *  Created on: Dec 1, 2018
 *      Author: tjc2c
 *
 *      Description:  This module implements all the command functions for
 *      the MSP432
 *
 *      */

#ifndef COMMANDPROCESSOR_H_
#define COMMANDPROCESSOR_H_


#include <ti/drivers/utils/List.h>

#include <pthread.h>
#include <time.h>


/* Alert definition */
typedef struct tempAlertThresholds_ {
    int ambient;
    int pulser;
    int powerSupply;
} TempAlertThresholds;


/**
 * Alert thresholds
 */
typedef struct alertThresholds_ {
    int low;
    int high;
} AlertThresholds;

/**
 * Suspend operations thresholds
 */
typedef  struct suspendOpsThresholds_ {
    int tempThresh;
    int currentThresh;
} SuspendOpsThresholds;

/**
 *  Pulse parameters/plans definitions
 *  */
typedef struct pulseParams_ {
    int width;
    int pri;
    int phaseDelay;
} PulseParams;

/**
 * Pulse plan definition
 */
typedef struct pulsePlan_ {
    int id;
    int startTime;
    uint32_t duration;
    PulseParams pulseParams;
    bool enabled;
} PulsePlan;

/**
 * Panel measurement definition
 */
typedef struct panelMeasurements_ {
    double panelCurrent;
    double panelVoltage;
    double panelPower;
} PanelMeasurementsType;


typedef struct pulsePlanListElem {
    List_Elem elem;
    PulsePlan pulsePlan;
} PulsePlanListElem;
/**
 * Pulse plan array
 */

List_List pulsePlanList;

/**
 * Command message queue identifier
 */
#define COMMAND_MSG_QUEUE "/command_msg_queue"
/**
 * Command response message queue identifier
 */
#define COMMAND_RES_QUEUE "/command_res_queue"

PanelMeasurementsType panelMeasurements;

PulseParams defaultPulseParams;
PulseParams manModePulseParams;
PulsePlan currentPulsePlan;
PulsePlan nextPulsePlan;
AlertThresholds currentAlerts;
AlertThresholds voltageAlerts;
AlertThresholds powerAlerts;
TempAlertThresholds tempAlerts;
SuspendOpsThresholds soThresholds;
char currentPulseMode[16];
#define MAX_ERROR_MSGS 5
#define MAX_ERROR_MSG_SIZE 128
int errorMsgCnt;
char errorMessages[MAX_ERROR_MSGS][MAX_ERROR_MSG_SIZE];
bool pulsePlanPropsSet;
bool pulseManModePropsSet;
bool pulseDefaultPropsSet;


/**
 * Processes the JSON command from the server
 *
 *    @param cmdJson        the command json string
 *    @param resultsJson    the command results json string
 *
 *    @return 0 if successful
 */
int processCommand(const char *cmdJson,char *resultsJson);

bool processPWM(const PulseParams *pulseP);

bool initCommandProcessor();

/**
 * Command processor pthread function
 */
void *commandProcessor(void *arg);
/**
 * Retrieves the panel voltage, current and power measurements
 * and stores them in the global panelMeasurements structure
 */
void getPanelMeasurements();

#endif /* COMMANDPROCESSOR_H_ */
