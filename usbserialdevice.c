/* --COPYRIGHT--,BSD
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/*
 *  ======== usbserialdevice.c ========
 */

#include <stdbool.h>
#include <string.h>

#include <ti/sysbios/knl/Swi.h>
/* For usleep() */
#include <ti/drivers/dpl/ClockP.h> 

/* Driver Header files */
#include <ti/display/Display.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>


/* Example/Board Header files */
//    longT #include "Board_usb.h"

/* USB Reference Module Header file */
#include "USBCDCD.h"
#include "usbserialdevice.h"
#include "console.h"
#include "parseCmd.h"



/*
 *  ======== usb_transmitFxn ========
 *  Task to transmit serial data.
 *
 *  This task periodically sends data to the USB host once it's connected.
 */

void *usb_transmitFxn(char *arg0)
{

   char *text = (char *)arg0;

        /* Block while the device is NOT connected to the USB */
        USBCDCD_waitForConnect(WAIT_FOREVER);

        USBCDCD_sendData(text, strlen(text)+1, WAIT_FOREVER);
        return NULL;
}


/*
 *  ======== receiveFxn ========
 *  Task to receive serial data.
 *
 *  This task will receive data when data is available and block while the
 *  device is not connected to the USB host or if no data was received.
 */

//unsigned char receiveFxn(void *arg0)
//void *receiveFxn(void *arg0)
void *usb_receiveFxn(void *arg0)
{
    unsigned int received;
    unsigned char data[32];
   // unsigned char data;
    char  cmds[80];
    cmd_token cmd_t;
    int i;

    received = 0;
    while (true) {

        /* Block while the device is NOT connected to the USB */
        USBCDCD_waitForConnect(WAIT_FOREVER);
        i= 0;

        do {       /* generate digits in reverse order */
            //received = USBCDCD_receiveData(data, 31, WAIT_FOREVER);
            received = USBCDCD_receiveData(data, 1, WAIT_FOREVER); //read one character
            if (received) {
                cmds[i++] = data[0];
                data[1] = '\0';
                usb_transmitFxn((char *)data);
              }
        }while (data[0] != 0x0D);;

        usb_transmitFxn((char *)("\n>\0"));  //enter newline and prompt >
        cmds[i++] = '\0'; //NULL =
        cmds[i++] = '\n'; //new line
        cmd_t = scanner(cmds);
        perform_cmd(cmd_t,cmds);

    }

 }


