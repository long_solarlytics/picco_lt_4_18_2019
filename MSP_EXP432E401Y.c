/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== MSP_EXP432E401Y.c ========
 *  This file is responsible for setting up the board specific items for the
 *  MSP_EXP432E401Y board.
 */
#include <stdint.h>
#include <stdlib.h>

#ifndef __MSP432E401Y__
#define __MSP432E401Y__
#endif
#include <ti/devices/msp432e4/inc/msp432.h>

#include <ti/devices/msp432e4/driverlib/adc.h>
#include <ti/devices/msp432e4/driverlib/interrupt.h>
#include <ti/devices/msp432e4/driverlib/pwm.h>
#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/udma.h>

#include <ti/drivers/Power.h>

#include "MSP_EXP432E401Y.h"

/*
 *  =============================== ADC ===============================
 */
#include <ti/drivers/ADC.h>
#include <ti/drivers/adc/ADCMSP432E4.h>

/* ADC objects */
ADCMSP432E4_Object adcMSP432E4Objects[MSP_EXP432E401Y_ADCCOUNT];

/* ADC configuration structure */
const ADCMSP432E4_HWAttrsV1 adcMSP432E4HWAttrs[MSP_EXP432E401Y_ADCCOUNT] = {
    {
        .adcPin = ADCMSP432E4_PE_3_A0,
        .refVoltage = ADCMSP432E4_VREF_INTERNAL,
        .adcModule = ADCMSP432E4_MOD0,
        .adcSeq = ADCMSP432E4_SEQ0
    },
    {
        .adcPin = ADCMSP432E4_PE_2_A1,
        .refVoltage = ADCMSP432E4_VREF_INTERNAL,
        .adcModule = ADCMSP432E4_MOD1,
        .adcSeq = ADCMSP432E4_SEQ0
    }
};

const ADC_Config ADC_config[MSP_EXP432E401Y_ADCCOUNT] = {
    {
        .fxnTablePtr = &ADCMSP432E4_fxnTable,
        .object = &adcMSP432E4Objects[MSP_EXP432E401Y_ADC0],
        .hwAttrs = &adcMSP432E4HWAttrs[MSP_EXP432E401Y_ADC0]
    },
    {
        .fxnTablePtr = &ADCMSP432E4_fxnTable,
        .object = &adcMSP432E4Objects[MSP_EXP432E401Y_ADC1],
        .hwAttrs = &adcMSP432E4HWAttrs[MSP_EXP432E401Y_ADC1]
    }
};

const uint_least8_t ADC_count = MSP_EXP432E401Y_ADCCOUNT;

/*
 *  ============================= Display =============================
 */
#include <ti/display/Display.h>
#include <ti/display/DisplayUart.h>
#include <Board.h>     //lTran
#define MAXPRINTLEN 1024

DisplayUart_Object displayUartObject;

static char displayBuf[MAXPRINTLEN];

#ifndef SOLARLYTICS_MCU

const DisplayUart_HWAttrs displayUartHWAttrs = {
    .uartIdx = MSP_EXP432E401Y_UART0,
    .baudRate = 115200,
    .mutexTimeout = (unsigned int)(-1),
    .strBuf = displayBuf,
    .strBufLen = MAXPRINTLEN
};
#else

const DisplayUart_HWAttrs displayUartHWAttrs = {
    .uartIdx = MSP_EXP432E401Y_UART7,
    .baudRate = 115200,
    .mutexTimeout = (unsigned int)(-1),
    .strBuf = displayBuf,
    .strBufLen = MAXPRINTLEN

};
#endif

#ifndef BOARD_DISPLAY_USE_UART_ANSI
#define BOARD_DISPLAY_USE_UART_ANSI 0
#endif

const Display_Config Display_config[] = {
    {
#  if (BOARD_DISPLAY_USE_UART_ANSI)
        .fxnTablePtr = &DisplayUartAnsi_fxnTable,
#  else /* Default to minimal UART with no cursor placement */
        .fxnTablePtr = &DisplayUartMin_fxnTable,
#  endif
        .object = &displayUartObject,
        .hwAttrs = &displayUartHWAttrs
    }
};

const uint_least8_t Display_count = sizeof(Display_config) /
                                    sizeof(Display_Config);

/*
 *  =============================== DMA ===============================
 */
#include <ti/drivers/dma/UDMAMSP432E4.h>

#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_ALIGN(dmaControlTable, 1024)
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma data_alignment=1024
#elif defined(__GNUC__)
__attribute__ ((aligned (1024)))
#endif
static tDMAControlTable dmaControlTable[64];

/*
 *  ======== dmaErrorFxn ========
 *  This is the handler for the uDMA error interrupt.
 */
static void dmaErrorFxn(uintptr_t arg)
{
    int status = uDMAErrorStatusGet();
    uDMAErrorStatusClear();

    /* Suppress unused variable warning */
    (void)status;

    while(1)
    {
        ;
    }
}

UDMAMSP432E4_Object udmaMSP432E4Object;

const UDMAMSP432E4_HWAttrs udmaMSP432E4HWAttrs = {
    .controlBaseAddr = (void *)dmaControlTable,
    .dmaErrorFxn = (UDMAMSP432E4_ErrorFxn)dmaErrorFxn,
    .intNum = INT_UDMAERR,
    .intPriority = (~0)
};

const UDMAMSP432E4_Config UDMAMSP432E4_config = {
    .object = &udmaMSP432E4Object,
    .hwAttrs = &udmaMSP432E4HWAttrs
};

/*
 *  =============================== General ===============================
 */
/*
 *  ======== MSP_EXP432E401Y_initGeneral ========
 */
void MSP_EXP432E401Y_initGeneral(void)
{
    Power_init();

    /* Grant the DMA access to all FLASH memory */
    FLASH_CTRL->PP |= FLASH_PP_DFA;

    /* Region start address - match FLASH start address */
    FLASH_CTRL->DMAST = 0x00000000;

    /*
     * Access to FLASH is granted to the DMA in 2KB regions.  The value
     * assigned to DMASZ is the amount of 2KB regions to which the DMA will
     * have access.  The value can be determined via the following:
     *     2 * (num_regions + 1) KB
     *
     * To grant full access to entire 1MB of FLASH:
     *     2 * (511 + 1) KB = 1024 KB (1 MB)
     */
    FLASH_CTRL->DMASZ = 511;
}

#ifdef ENABLE_EMAC
/*
 *  =============================== EMAC ===============================
 */
#include <ti/drivers/emac/EMACMSP432E4.h>

/*
 *  Required by the Networking Stack (NDK). This array must be NULL terminated.
 *  This can be removed if NDK is not used.
 *  Double curly braces are needed to avoid GCC bug #944572
 *  https://bugs.launchpad.net/gcc-linaro/+bug/944572
 */
NIMU_DEVICE_TABLE_ENTRY NIMUDeviceTable[2] = {
    {
        /* Default: use Ethernet driver */
        .init = EMACMSP432E4_NIMUInit
    },
    {NULL}
};

/*
 *  EMAC configuration structure
 *  Set user/company specific MAC octates. The following sets the address
 *  to ff-ff-ff-ff-ff-ff. Users need to change this to make the label on
 *  their boards.
 */
unsigned char macAddress[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
///unsigned char macAddress[6] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0x9b};
const EMACMSP432E4_HWAttrs EMACMSP432E4_hwAttrs = {
    .baseAddr = EMAC0_BASE,
    .intNum = INT_EMAC0,
    .intPriority = (~0),
    .led0Pin = EMACMSP432E4_PF0_EN0LED0,
    .led1Pin = EMACMSP432E4_PF4_EN0LED1,

    .macAddress = macAddress
};
#endif

/*
 *  =============================== GPIO ===============================
 */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/gpio/GPIOMSP432E4.h>

/*
 * Array of Pin configurations
 * NOTE: The order of the pin configurations must coincide with what was
 *       defined in MSP_EXP432E401Y.h
 * NOTE: Pins not used for interrupts should be placed at the end of the
 *       array.  Callback entries can be omitted from callbacks array to
 *       reduce memory usage.
 */


GPIO_PinConfig gpioPinConfigs[] = {


      /* MSP_EXP432E401Y_HOST_IRQ */
      GPIOMSP432E4_PM7 | GPIO_CFG_IN_PD | GPIO_CFG_IN_INT_RISING,

#ifndef SOLARLYTICS_MCU
      /* Output pins */
      /* MSP_EXP432E401Y_USR_D1 */
      GPIOMSP432E4_PN1 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH |
      GPIO_CFG_OUT_LOW,
      /* MSP_EXP432E401Y_USR_D2 */
      GPIOMSP432E4_PN0 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH |
      GPIO_CFG_OUT_LOW,
#else
      /* Output pins */
      //MSP_EXP432E401Y_GPIO_LED0_PN2
      GPIOMSP432E4_PN2 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
      //MSP_EXP432E401Y_GPIO_LED1_PN3
      GPIOMSP432E4_PN3 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
#endif
      /* MSP_EXP432E401Y_nHIB_pin */
      GPIOMSP432E4_PJ0 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_OD_NOPULL,

#ifndef SOLARLYTICS_MCU
      /* MSP_EXP432E401Y_CS_pin */
      GPIOMSP432E4_PP5 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH |
      GPIO_CFG_OUT_HIGH,

#else
      /* MSP_EXP432E401Y_CS_pin */
       GPIOMSP432E4_PB4 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH |
       GPIO_CFG_OUT_HIGH,

#endif

#ifdef WIFI_EXTERNAL_PROG
       /* MSP_EXP432E401Y_WIFI_RX */
       GPIOMSP432E4_PP1 | GPIO_CFG_INPUT,
       GPIOMSP432E4_PP0 | GPIO_CFG_INPUT,
#endif
#ifndef WIFI_USE_SPI1
       /*
        * The Solarlytic MCU designed with SPI1 to be used for WIFI 31120. However, SPI1 found to be failed in this application.
        * SPI3 is used in instead. For ease of rework, SPI_RX, SPI_TX and SPI_CLK of SPI1 are wired together respectively.
        * SPI1 becomes unused, and its signals SPI1_RX, SPI1_TX, and SPI1_CLK are configured as inputs.
        */
       GPIOMSP432E4_PE5 | GPIO_CFG_INPUT,        //MSP_EXP432E401Y_SPI1_RX,
       GPIOMSP432E4_PE4 | GPIO_CFG_INPUT,          // MSP_EXP432E401Y_SPI1_TX,
       GPIOMSP432E4_PB5 | GPIO_CFG_INPUT,           // MSP_EXP432E401Y_SPI1_CLK,
#endif

        MSP_EXP432E401Y_GPIOCOUNT


};

/*
 * Array of callback function pointers
 * NOTE: The order of the pin configurations must coincide with what was
 *       defined in MSP_EXP432E401Y.h
 * NOTE: Pins not used for interrupts can be omitted from callbacks array to
 *       reduce memory usage (if placed at end of gpioPinConfigs array).
 */
GPIO_CallbackFxn gpioCallbackFunctions[] = {
    NULL,  /* MSP_EXP432E401Y_USR_SW1 */
    NULL,  /* MSP_EXP432E401Y_USR_SW2 */
    NULL,  /* MSP_EXP432E401Y_HOST_IRQ */
};

/* The device-specific GPIO_config structure */
const GPIOMSP432E4_Config GPIOMSP432E4_config = {
    .pinConfigs = (GPIO_PinConfig *)gpioPinConfigs,
    .callbacks = (GPIO_CallbackFxn *)gpioCallbackFunctions,
    .numberOfPinConfigs = sizeof(gpioPinConfigs) / sizeof(GPIO_PinConfig),
    .numberOfCallbacks = sizeof(gpioCallbackFunctions) /
                         sizeof(GPIO_CallbackFxn),
    .intPriority = (~0)
};

/*
 *  =============================== I2C ===============================
 */
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CMSP432E4.h>

I2CMSP432E4_Object i2cMSP432E4Objects[MSP_EXP432E401Y_I2CCOUNT];

const I2CMSP432E4_HWAttrs i2cMSP432E4HWAttrs[MSP_EXP432E401Y_I2CCOUNT] = {
    {
        .baseAddr = I2C0_BASE,
        .intNum = INT_I2C0,
        .intPriority = (~0),
        .sclPin = I2CMSP432E4_PB2_I2C0SCL,
        .sdaPin = I2CMSP432E4_PB3_I2C0SDA
    },
};

const I2C_Config I2C_config[MSP_EXP432E401Y_I2CCOUNT] = {
    {
        .fxnTablePtr = &I2CMSP432E4_fxnTable,
        .object = &i2cMSP432E4Objects[MSP_EXP432E401Y_I2C0],
        .hwAttrs = &i2cMSP432E4HWAttrs[MSP_EXP432E401Y_I2C0]
    },
};

const uint_least8_t I2C_count = MSP_EXP432E401Y_I2CCOUNT;

/*
 *  =============================== NVS ===============================
 */
#include <ti/drivers/NVS.h>
#include <ti/drivers/nvs/NVSMSP432E4.h>

#define SECTORSIZE       (0x4000)
#define NVS_REGIONS_BASE (0xF8000)
#define REGIONSIZE       (SECTORSIZE * 2)

/*
 * Reserve flash sectors for NVS driver use
 * by placing an uninitialized byte array
 * at the desired flash address.
 */
#if defined(__TI_COMPILER_VERSION__)

/*
 * Place uninitialized array at NVS_REGIONS_BASE
 */
#pragma LOCATION(flashBuf, NVS_REGIONS_BASE);
#pragma NOINIT(flashBuf);
static char flashBuf[REGIONSIZE];

#elif defined(__IAR_SYSTEMS_ICC__)

/*
 * Place uninitialized array at NVS_REGIONS_BASE
 */
__no_init static char flashBuf[REGIONSIZE] @ NVS_REGIONS_BASE;

#elif defined(__GNUC__)

/*
 * Place the flash buffers in the .nvs section created in the gcc linker file.
 * The .nvs section enforces alignment on a sector boundary but may
 * be placed anywhere in flash memory.  If desired the .nvs section can be set
 * to a fixed address by changing the following in the gcc linker file:
 *
 * .nvs (FIXED_FLASH_ADDR) (NOLOAD) : AT (FIXED_FLASH_ADDR) {
 *      *(.nvs)
 * } > REGION_TEXT
 */
__attribute__ ((section (".nvs")))
static char flashBuf[REGIONSIZE];

#endif

NVSMSP432E4_Object nvsMSP432E4Objects[MSP_EXP432E401Y_NVSCOUNT];

const NVSMSP432E4_HWAttrs nvsMSP432E4HWAttrs[MSP_EXP432E401Y_NVSCOUNT] = {
    {
        .regionBase = (void *) flashBuf,
        .regionSize = REGIONSIZE,
    },
};

const NVS_Config NVS_config[MSP_EXP432E401Y_NVSCOUNT] = {
    {
        .fxnTablePtr = &NVSMSP432E4_fxnTable,
        .object = &nvsMSP432E4Objects[MSP_EXP432E401Y_NVSMSP432E40],
        .hwAttrs = &nvsMSP432E4HWAttrs[MSP_EXP432E401Y_NVSMSP432E40],
    },
};

const uint_least8_t NVS_count = MSP_EXP432E401Y_NVSCOUNT;


/*
 *  =============================== Timer ===============================
 */
#include <ti/drivers/Timer.h>
#include <ti/drivers/timer/TimerMSP432E4.h>

TimerMSP432E4_Object timerMSP432E4Objects[MSP_EXP432E401Y_TIMERCOUNT];

const TimerMSP432E4_HWAttrs timerMSP432E4HWAttrs[MSP_EXP432E401Y_TIMERCOUNT] = {
    {
        .baseAddress = TIMER2_BASE,
        .subTimer = TimerMSP432E4_timer32,
        .intNum = INT_TIMER2A,
        .intPriority = ~0
    },
    {
        .baseAddress = TIMER1_BASE,
      ////  .subTimer = TimerMSP432E4_timer16A,
        .subTimer = TimerMSP432E4_timer32,
        .intNum = INT_TIMER1A,
        .intPriority = ~0
    },
    {
         .baseAddress = TIMER1_BASE,
         .subTimer = TimerMSP432E4_timer16B,
         .intNum = INT_TIMER1B,
         .intPriority = ~0
    },
};

const Timer_Config Timer_config[MSP_EXP432E401Y_TIMERCOUNT] = {
    {
        .fxnTablePtr = &TimerMSP432E4_fxnTable,
        .object = &timerMSP432E4Objects[MSP_EXP432E401Y_TIMER0],
        .hwAttrs = &timerMSP432E4HWAttrs[MSP_EXP432E401Y_TIMER0]
    },
    {
        .fxnTablePtr = &TimerMSP432E4_fxnTable,
        .object = &timerMSP432E4Objects[MSP_EXP432E401Y_TIMER1],
        .hwAttrs = &timerMSP432E4HWAttrs[MSP_EXP432E401Y_TIMER1]
    },
    {
        .fxnTablePtr = &TimerMSP432E4_fxnTable,
        .object = &timerMSP432E4Objects[MSP_EXP432E401Y_TIMER2],
        .hwAttrs = &timerMSP432E4HWAttrs[MSP_EXP432E401Y_TIMER2]
    },
};

const uint_least8_t Timer_count = MSP_EXP432E401Y_TIMERCOUNT;

/*
 *  =============================== Power ===============================
 */
#include <ti/drivers/power/PowerMSP432E4.h>
const PowerMSP432E4_Config PowerMSP432E4_config = {
    .policyFxn = &PowerMSP432E4_sleepPolicy,
    .enablePolicy = true
};



/*
 *  =============================== PWM ===============================
 */

#include <ti/drivers/PWM.h>
#include <ti/drivers/pwm/PWMMSP432E4.h>
#include <ti/devices/msp432e4/driverlib/pwm.h>

const uint_least8_t PWM_count = MSP_EXP432E401Y_PWMCOUNT;
//uint_least8_t PWM_count = 4;
//#define PWM_count 4
/*
 *  ======== pwmMSP432E4Objects ========
 */

PWMMSP432E4_Object pwmMSP432E4Objects[MSP_EXP432E401Y_PWMCOUNT];

/*
 *  ======== pwmMSP432E4HWAttrs ========
 */

const PWMMSP432E4_HWAttrs pwmMSP432E4HWAttrs[MSP_EXP432E401Y_PWMCOUNT] = {
    /* Board_PWM1 */
    /* LaunchPad LED D4 (Red) */
    {
        .pwmBaseAddr = PWM0_BASE,
        .pwmOutput   = PWM_OUT_1,
       .pwmGenOpts = PWM_GEN_MODE_DOWN | PWM_GEN_MODE_DBG_RUN,
        .pinConfig   = PWMMSP432E4_PF1_M0PWM1, /* PF1 */
    },
    /* Board_PWM3 */
    {
        .pwmBaseAddr = PWM0_BASE,
        .pwmOutput   = PWM_OUT_3,
        .pwmGenOpts = PWM_GEN_MODE_DOWN | PWM_GEN_MODE_DBG_RUN,
        .pinConfig   = PWMMSP432E4_PF3_M0PWM3, /* PF3 */
    },
    /* Board_PWM4 */
    {
        .pwmBaseAddr = PWM0_BASE,
        .pwmOutput   = PWM_OUT_4,
        .pwmGenOpts  = PWM_GEN_MODE_DOWN |
                       PWM_GEN_MODE_DBG_RUN |
                       PWM_GEN_MODE_NO_SYNC,
        .pinConfig   = PWMMSP432E4_PG0_M0PWM4, /* PG0 */
    },
    /* Board_PWM7 */
    {
        .pwmBaseAddr = PWM0_BASE,
        .pwmOutput   = PWM_OUT_7,
        .pwmGenOpts  = PWM_GEN_MODE_DOWN |
                       PWM_GEN_MODE_DBG_RUN |
                       PWM_GEN_MODE_NO_SYNC,
        .pinConfig   = PWMMSP432E4_PK5_M0PWM7, /* PK5 */
    },
};

/*
 *  ======== PWM_config ========
 */
///MSP_EXP432E401Y_PWM1

const PWM_Config PWM_config[MSP_EXP432E401Y_PWMCOUNT] = {
    /* Board_PWM1 */

    {
        .fxnTablePtr = &PWMMSP432E4_fxnTable,
        .object = &pwmMSP432E4Objects[Board_PWM1],
        .hwAttrs = &pwmMSP432E4HWAttrs[Board_PWM1]
    },
    /* Board_PWM3 */
    {
        .fxnTablePtr = &PWMMSP432E4_fxnTable,
        .object = &pwmMSP432E4Objects[Board_PWM3],
        .hwAttrs = &pwmMSP432E4HWAttrs[Board_PWM3]
    },
    /* Board_PWM4 */
    {
        .fxnTablePtr = &PWMMSP432E4_fxnTable,
        .object = &pwmMSP432E4Objects[Board_PWM4],
        .hwAttrs = &pwmMSP432E4HWAttrs[Board_PWM4]
    },
    /* Board_PWM7 */
    {
        .fxnTablePtr = &PWMMSP432E4_fxnTable,
        .object = &pwmMSP432E4Objects[Board_PWM7],
        .hwAttrs = &pwmMSP432E4HWAttrs[Board_PWM7]
    },
};




/*
 *  =============================== SDFatFS ===============================
 */
#include <ti/drivers/SD.h>
#include <ti/drivers/SDFatFS.h>

/*
 * Note: The SDFatFS driver provides interface functions to enable FatFs
 * but relies on the SD driver to communicate with SD cards.  Opening a
 * SDFatFs driver instance will internally try to open a SD driver instance
 * reusing the same index number (opening SDFatFs driver at index 0 will try to
 * open SD driver at index 0).  This requires that all SDFatFs driver instances
 * have an accompanying SD driver instance defined with the same index.  It is
 * acceptable to have more SD driver instances than SDFatFs driver instances
 * but the opposite is not supported & the SDFatFs will fail to open.
 */
SDFatFS_Object sdfatfsObjects[MSP_EXP432E401Y_SDFatFSCOUNT];

const SDFatFS_Config SDFatFS_config[MSP_EXP432E401Y_SDFatFSCOUNT] = {
    {
        .object = &sdfatfsObjects[MSP_EXP432E401Y_SDFatFS0]
    }
};

const uint_least8_t SDFatFS_count = MSP_EXP432E401Y_SDFatFSCOUNT;

/*
 *  =============================== SPI ===============================
 */
#include <ti/drivers/SPI.h>
#include <ti/drivers/spi/SPIMSP432E4DMA.h>

SPIMSP432E4DMA_Object spiMSP432E4DMAObjects[MSP_EXP432E401Y_SPICOUNT];

/*
 * NOTE: The SPI instances below can be used by the SD driver to communicate
 * with a SD card via SPI.  The 'defaultTxBufValue' fields below are set to
 * (~0) to satisfy the SDSPI driver requirement.
 */
const SPIMSP432E4DMA_HWAttrs spiMSP432E4DMAHWAttrs[MSP_EXP432E401Y_SPICOUNT] =
{
#ifdef WIFI_USE_SPI1
    {
         .baseAddr = SSI1_BASE,
         .intNum = INT_SSI1,
         .intPriority = (~0),
         .defaultTxBufValue = (~0),
         .minDmaTransferSize = 10,
         .rxDmaChannel = UDMA_CH10_SSI1RX,
         .txDmaChannel = UDMA_CH11_SSI1TX,
         .clkPinMask = SPIMSP432E4_PB5_SSI1CLK,
        ///// .fssPinMask = SPIMSP432E4_PB4_SSI1FSS,
         .xdat0PinMask = SPIMSP432E4_PE4_SSI1XDAT0, //tx
         .xdat1PinMask = SPIMSP432E4_PE5_SSI1XDAT1  //rx
     },

#endif


#ifdef WIFI_USE_SPI2
     {
         .baseAddr = SSI2_BASE,
         .intNum = INT_SSI2,
         .intPriority = (~0),
         .defaultTxBufValue = (~0),
         .rxDmaChannel = UDMA_CH12_SSI2RX,
         .txDmaChannel = UDMA_CH13_SSI2TX,
         .minDmaTransferSize = 10,
         .clkPinMask = SPIMSP432E4_PD3_SSI2CLK,
         .fssPinMask = SPIMSP432E4_PD2_SSI2FSS,
         .xdat0PinMask = SPIMSP432E4_PD1_SSI2XDAT0,
         .xdat1PinMask = SPIMSP432E4_PD0_SSI2XDAT1
     },
 #endif

#ifdef WIFI_USE_SPI3
     {
      .baseAddr = SSI3_BASE,
      .intNum = INT_SSI3,
      .intPriority = (~0),
      .defaultTxBufValue = (~0),
      .minDmaTransferSize = 10,
      .rxDmaChannel = UDMA_CH14_SSI3RX,
      .txDmaChannel = UDMA_CH15_SSI3TX,
      .clkPinMask = SPIMSP432E4_PQ0_SSI3CLK,
      .fssPinMask = SPIMSP432E4_PQ1_SSI3FSS,
      .xdat0PinMask = SPIMSP432E4_PQ2_SSI3XDAT0,
      .xdat1PinMask = SPIMSP432E4_PQ3_SSI3XDAT1
     }

#endif

};

const SPI_Config SPI_config[MSP_EXP432E401Y_SPICOUNT] = {

#ifdef WIFI_USE_SPI1
   {
        .fxnTablePtr = &SPIMSP432E4DMA_fxnTable,
        .object = &spiMSP432E4DMAObjects[MSP_EXP432E401Y_SPI1],
        .hwAttrs = &spiMSP432E4DMAHWAttrs[MSP_EXP432E401Y_SPI1]
   },
#endif
#ifdef WIFI_USE_SPI2
    {
        .fxnTablePtr = &SPIMSP432E4DMA_fxnTable,
        .object = &spiMSP432E4DMAObjects[MSP_EXP432E401Y_SPI2],
        .hwAttrs = &spiMSP432E4DMAHWAttrs[MSP_EXP432E401Y_SPI2]

    },
#endif

#ifdef WIFI_USE_SPI3
    {
        .fxnTablePtr = &SPIMSP432E4DMA_fxnTable,
        .object = &spiMSP432E4DMAObjects[MSP_EXP432E401Y_SPI3],
        .hwAttrs = &spiMSP432E4DMAHWAttrs[MSP_EXP432E401Y_SPI3]
    },
#endif
};

const uint_least8_t SPI_count = MSP_EXP432E401Y_SPICOUNT;

/*
 *  =============================== UART ===============================
 */
#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTMSP432E4.h>

UARTMSP432E4_Object uartMSP432E4Objects[MSP_EXP432E401Y_UARTCOUNT];
unsigned char uartMSP432E4RingBuffer[MSP_EXP432E401Y_UARTCOUNT][32];

/* UART configuration structure */
const UARTMSP432E4_HWAttrs uartMSP432E4HWAttrs[MSP_EXP432E401Y_UARTCOUNT] = {
#ifndef SOLARLYTICS_MCU

    {
        .baseAddr = UART0_BASE,
        .intNum = INT_UART0,
        .intPriority = (~0),
        .flowControl = UARTMSP432E4_FLOWCTRL_NONE,
        .ringBufPtr = uartMSP432E4RingBuffer[MSP_EXP432E401Y_UART0],
        .ringBufSize = sizeof(uartMSP432E4RingBuffer[MSP_EXP432E401Y_UART0]),
        .rxPin = UARTMSP432E4_PA0_U0RX,
        .txPin = UARTMSP432E4_PA1_U0TX,
        .ctsPin = UARTMSP432E4_PIN_UNASSIGNED,
        .rtsPin = UARTMSP432E4_PIN_UNASSIGNED
    },

#else
    {
           .baseAddr = UART7_BASE,
           .intNum = INT_UART7,
           .intPriority = (~0),
           .flowControl = UARTMSP432E4_FLOWCTRL_NONE,
           .ringBufPtr = uartMSP432E4RingBuffer[MSP_EXP432E401Y_UART7],
           .ringBufSize = sizeof(uartMSP432E4RingBuffer[MSP_EXP432E401Y_UART7]),
           .rxPin = UARTMSP432E4_PC4_U7RX,
           .txPin = UARTMSP432E4_PC5_U7TX,
           .ctsPin = UARTMSP432E4_PIN_UNASSIGNED,
           .rtsPin = UARTMSP432E4_PIN_UNASSIGNED
       },
#ifndef WIFI_EXTERNAL_PROG
    {
          .baseAddr = UART6_BASE,
          .intNum = INT_UART6,
          .intPriority = (~0),
          .flowControl = UARTMSP432E4_FLOWCTRL_NONE,
          .ringBufPtr = uartMSP432E4RingBuffer[MSP_EXP432E401Y_UART6],
          .ringBufSize = sizeof(uartMSP432E4RingBuffer[MSP_EXP432E401Y_UART6]),
          .rxPin = UARTMSP432E4_PP0_U6RX,
          .txPin = UARTMSP432E4_PP1_U6TX,
          .ctsPin = UARTMSP432E4_PIN_UNASSIGNED,
          .rtsPin = UARTMSP432E4_PIN_UNASSIGNED
      },
#endif

#endif
};

const UART_Config UART_config[MSP_EXP432E401Y_UARTCOUNT] = {
#ifndef SOLARLYTICS_MCU
    {
        .fxnTablePtr = &UARTMSP432E4_fxnTable,
        .object = &uartMSP432E4Objects[MSP_EXP432E401Y_UART0],
        .hwAttrs = &uartMSP432E4HWAttrs[MSP_EXP432E401Y_UART0]
    },
#if 0
    {
         .fxnTablePtr = &UARTMSP432E4_fxnTable,
         .object = &uartMSP432E4Objects[MSP_EXP432E401Y_UART6],
         .hwAttrs = &uartMSP432E4HWAttrs[MSP_EXP432E401Y_UART6]
     },
#endif
#else
    {
        .fxnTablePtr = &UARTMSP432E4_fxnTable,
        .object = &uartMSP432E4Objects[MSP_EXP432E401Y_UART7],
        .hwAttrs = &uartMSP432E4HWAttrs[MSP_EXP432E401Y_UART7]
    },
#ifndef WIFI_EXTERNAL_PROG
    {
          .fxnTablePtr = &UARTMSP432E4_fxnTable,
          .object = &uartMSP432E4Objects[MSP_EXP432E401Y_UART6],
          .hwAttrs = &uartMSP432E4HWAttrs[MSP_EXP432E401Y_UART6]
      }
#endif
#endif
};

const uint_least8_t UART_count = MSP_EXP432E401Y_UARTCOUNT;

/*
 *  =============================== Watchdog ===============================
 */
#include <ti/drivers/Watchdog.h>
#include <ti/drivers/watchdog/WatchdogMSP432E4.h>

WatchdogMSP432E4_Object watchdogMSP432E4Objects[MSP_EXP432E401Y_WATCHDOGCOUNT];

const WatchdogMSP432E4_HWAttrs watchdogMSP432E4HWAttrs[
    MSP_EXP432E401Y_WATCHDOGCOUNT] = {
    {
        .baseAddr = WATCHDOG0_BASE,
        .intNum = INT_WATCHDOG,
        .intPriority = (~0),
        .reloadValue = 80000000 // 1 second period at default CPU clock freq
    },
};

const Watchdog_Config Watchdog_config[MSP_EXP432E401Y_WATCHDOGCOUNT] = {
    {
        .fxnTablePtr = &WatchdogMSP432E4_fxnTable,
        .object = &watchdogMSP432E4Objects[MSP_EXP432E401Y_WATCHDOG0],
        .hwAttrs = &watchdogMSP432E4HWAttrs[MSP_EXP432E401Y_WATCHDOG0]
    },
};

const uint_least8_t Watchdog_count = MSP_EXP432E401Y_WATCHDOGCOUNT;

/*
 *  =============================== WiFi ===============================
 *
 * This is the configuration structure for the WiFi module that will be used
 * as part of the SimpleLink SDK WiFi plugin. These are configured for SPI mode.
 * Any changes here will need to be configured on the CC31xx device as well
 */
#include <ti/drivers/net/wifi/porting/SIMPLELINKWIFI.h>

const SIMPLELINKWIFI_HWAttrsV1 wifiSimplelinkHWAttrs =
{
#ifdef WIFI_USE_SPI1
    .spiIndex = MSP_EXP432E401Y_SPI1,
#endif
#ifdef WIFI_USE_SPI2
    .spiIndex = MSP_EXP432E401Y_SPI2,
#endif
#ifdef WIFI_USE_SPI3
    .spiIndex = MSP_EXP432E401Y_SPI3,
#endif
    .hostIRQPin = MSP_EXP432E401Y_HOST_IRQ,
    .nHIBPin = MSP_EXP432E401Y_nHIB_pin,
    .csPin = MSP_EXP432E401Y_CS_pin,
    .maxDMASize = 1024,
    .spiBitRate = 3000000
};

const uint_least8_t WiFi_count = 1;

const WiFi_Config WiFi_config[1] =
{
    {
        .hwAttrs = &wifiSimplelinkHWAttrs,
    }
};



#include <ti/devices/msp432e4/driverlib/types.h>
#include <string.h>
#include <stdio.h>

/**
 * Reads contents of the UNIQUEIDx SYSCTL registers and populates the supplied
 * uniqueId character array
 *     @param uniqueId - character array where the unique id string will be placed
 */
void getUniqueId(char *uniqueId) {

    uint32_t uid0 = SYSCTL->UNIQUEID0;
    uint32_t uid1 = SYSCTL->UNIQUEID1;
    uint32_t uid2 = SYSCTL->UNIQUEID2;
    uint32_t uid3 = SYSCTL->UNIQUEID3;

    sprintf(uniqueId,"%0X%0X%0X%0X",uid3,uid2,uid1,uid0);
}


/**
 * Reads the contents of the DIDx SYSCTL registers and populates the supplied
 * deviceId character array
 *     @param deviceId - character array where the device id string will be placed
 */
void getDeviceId(char *deviceId) {

    uint32_t did0 = SYSCTL->DID0;
    uint32_t did1 = SYSCTL->DID1;

    sprintf(deviceId,"%0X%0X",did1,did0);
}

/**
 * Reads the contents of the EMAC0 ADDR0H/L registers and populates the supplied
 * mac address character array
 *     @param macAddr - character array where the mac address string will be placed
 */
void getMacAddress(char *macAddr) {

    uint16_t addr0h = EMAC0->ADDR0H & 0x0000FFFF;
    uint32_t addr0l = EMAC0->ADDR0L;

    sprintf(macAddr,"%0hX%0X",addr0h,addr0l);
}



