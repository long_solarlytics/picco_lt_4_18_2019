/*
 * pwm.c
 *
 *  Created on: Feb 28, 2019
 *      Author: LTran
 */



#include <stdint.h>
#include <pwm.h>

/* POSIX Header files */
#include <pthread.h>
#include <ti/drivers/PWM.h>
#include "Board.h"
#include <ti/drivers/dpl/ClockP.h>


#include <ti/devices/msp432e4/driverlib/inc/hw_pwm.h>
#include <ti/devices/msp432e4/driverlib/types.h>
#include <ti/devices/msp432e4/driverlib/pwm.h>
#include <ti/devices/msp432e4/inc/msp432e411y.h>


//*****************************************************************************
//
//! Configures a PWM generator.
//!
//! \param ui32Base is the base address of the PWM module.
//! \param ui32Gen is the PWM generator to configure.  This parameter must be
//! one of \b PWM_GEN_0, \b PWM_GEN_1, \b PWM_GEN_2, or \b PWM_GEN_3.
//! \param ui32Config is the configuration for the PWM generator.
//!
//! This function is used to set the mode of operation for a PWM generator.
//! The counting mode, synchronization mode, and debug behavior are all
//! configured.  After configuration, the generator is left in the disabled
//! state.
//!
//! A PWM generator can count in two different modes:  count down mode or count
//! up/down mode.  In count down mode, it counts from a value down to zero,
//! and then resets to the preset value, producing left-aligned PWM
//! signals (that is, the rising edge of the two PWM signals produced by the
//! generator occur at the same time).  In count up/down mode, it counts up
//! from zero to the preset value, counts back down to zero, and then repeats
//! the process, producing center-aligned PWM signals (that is,
//! the middle of the high/low period of the PWM signals produced by the
//! generator occurs at the same time).
//!
//! When the PWM generator parameters (period and pulse width) are modified,
//! their effect on the output PWM signals can be delayed.  In synchronous
//! mode, the parameter updates are not applied until a synchronization event
//! occurs.  This mode allows multiple parameters to be modified and take
//! effect simultaneously, instead of one at a time.  Additionally, parameters
//! to multiple PWM generators in synchronous mode can be updated
//! simultaneously, allowing them to be treated as if they were a unified
//! generator.  In non-synchronous mode, the parameter updates are not delayed
//! until a synchronization event.  In either mode, the parameter updates only
//! occur when the counter is at zero to help prevent oddly formed PWM signals
//! during the update (that is, a PWM pulse that is too short or too long).
//!
//! The PWM generator can either pause or continue running when the processor
//! is stopped via the debugger.  If configured to pause, it continues to
//! count until it reaches zero, at which point it pauses until the
//! processor is restarted.  If configured to continue running, it keeps
//! counting as if nothing had happened.
//!
//! The \e ui32Config parameter contains the desired configuration.  It is the
//! logical OR of the following:
//!
//! - \b PWM_GEN_MODE_DOWN or \b PWM_GEN_MODE_UP_DOWN to specify the counting
//!   mode
//! - \b PWM_GEN_MODE_SYNC or \b PWM_GEN_MODE_NO_SYNC to specify the counter
//!   load and comparator update synchronization mode
//! - \b PWM_GEN_MODE_DBG_RUN or \b PWM_GEN_MODE_DBG_STOP to specify the debug
//!   behavior
//! - \b PWM_GEN_MODE_GEN_NO_SYNC, \b PWM_GEN_MODE_GEN_SYNC_LOCAL, or
//!   \b PWM_GEN_MODE_GEN_SYNC_GLOBAL to specify the update synchronization
//!   mode for generator counting mode changes
//! - \b PWM_GEN_MODE_DB_NO_SYNC, \b PWM_GEN_MODE_DB_SYNC_LOCAL, or
//!   \b PWM_GEN_MODE_DB_SYNC_GLOBAL to specify the deadband parameter
//!   synchronization mode
//! - \b PWM_GEN_MODE_FAULT_LATCHED or \b PWM_GEN_MODE_FAULT_UNLATCHED to
//!   specify whether fault conditions are latched or not
//! - \b PWM_GEN_MODE_FAULT_MINPER or \b PWM_GEN_MODE_FAULT_NO_MINPER to
//!   specify whether minimum fault period support is required
//! - \b PWM_GEN_MODE_FAULT_EXT or \b PWM_GEN_MODE_FAULT_LEGACY to specify
//!   whether extended fault source selection support is enabled or not
//!
//! Setting \b PWM_GEN_MODE_FAULT_MINPER allows an application to set the
//! minimum duration of a PWM fault signal.  Faults are signaled for at
//! least this time even if the external fault pin deasserts earlier.  Care
//! should be taken when using this mode because during the fault signal
//! period, the fault interrupt from the PWM generator remains asserted.  The
//! fault interrupt handler may, therefore, reenter immediately if it exits
//! prior to expiration of the fault timer.
//!
//! \note Changes to the counter mode affect the period of the PWM signals
//! produced.  PWMGenPeriodSet() and PWMPulseWidthSet() should be called after
//! any changes to the counter mode of a generator.
//!
//! \return None.
//



#define PWM_GEN_BADDR(_mod_, _gen_)                                           \
                                ((_mod_) + (_gen_))

#define PWM_OUT_BADDR(_mod_, _out_)                                           \
                                ((_mod_) + ((_out_) & 0xFFFFFFC0))


PWM_Handle pwm1 = NULL;
PWM_Handle pwm3 = NULL;
PWM_Handle pwm4 = NULL;
PWM_Handle pwm7 = NULL;

//*****************************************************************************
void
redoPWM_Init(void)

{
    //
    // Check the arguments.
    //
    uint32_t ui32Base = PWM0_BASE;

    uint32_t ui32Gen = PWM_GEN_0;

    ui32Gen = PWM_GEN_BADDR(ui32Base, ui32Gen);

    /*
     * config PWM Gnerator 0
     */

    HWREG(ui32Gen + PWM_O_X_GENA) = (PWM_X_GENA_ACTLOAD_ONE |
                                      PWM_X_GENA_ACTCMPAD_ZERO);
    HWREG(ui32Gen + PWM_O_X_GENB) = (PWM_X_GENB_ACTLOAD_ONE |
                                     // PWM_X_GENB_ACTCMPBD_ZERO);
                                         PWM_X_GENB_ACTCMPBD_ZERO);


    /*
     * config PWM Gnerator 1
     */

    ui32Gen = PWM_GEN_1;

    ui32Gen = PWM_GEN_BADDR(ui32Base, ui32Gen);

    HWREG(ui32Gen + PWM_O_X_GENA) = (
            PWM_0_GENA_ACTCMPBD_ZERO | //counter down match comparatorB, gen 0
            PWM_0_GENA_ACTCMPAD_ONE ); //counter down match comparator A, generate 1

    HWREG(ui32Gen + PWM_O_X_GENB) = (
            PWM_0_GENB_ACTCMPAD_ONE |
            PWM_0_GENB_ACTCMPBD_ZERO ); //counter down match comparatorB, gen 0
                  // PWM_0_GENB_ACTCMPAD_ONE ); //counter down match comparator A, generate 1

}

void
PWMPulseWidthSetWithDelay(uint32_t ui32Base, uint32_t ui32PWMOut,
                 uint32_t ui32Width, uint32_t ui32Delay)
{
    uint32_t ui32GenBase, ui32Reg;

    //
    // Check the arguments.
    //
#if 0
    ASSERT(ui32Base == PWM0_BASE);
    ASSERT(_PWMOutValid(ui32PWMOut));
#endif
    //
    // Compute the generator's base address.
    //
    ui32GenBase = PWM_OUT_BADDR(ui32Base, ui32PWMOut);

    //
    // Get the period.
    //
    ui32Reg = HWREG(ui32GenBase + PWM_O_X_LOAD);

    //
    // Make sure the width is not too large.
    //
#if 0
    ASSERT(ui32Width < ui32Reg);
#endif
    //
    // Compute the compare value.
    //
    ui32Reg = ui32Reg - ui32Delay;
    HWREG(ui32GenBase + PWM_O_X_CMPA) = ui32Reg;
    ui32Reg = ui32Reg - ui32Width;
    HWREG(ui32GenBase + PWM_O_X_CMPB) = ui32Reg;

    HWREG(PWM0_BASE + PWM_O_INVERT) =0;         //not invert pwm output

    HWREG(PWM0_BASE + PWM_O_SYNC) =3;         //reset counter pwm 0 and 1

}

void pwm_init(void)

{

    uint32_t   pwmPeriod = 15000;      //in ns
    pwmPeriod = (pwmPeriod *12)/100;    //in cycle
    /*
     * freq = 120Mhz = 120,000,000 cycles in 1s
     *               = 120,000 cycles in 1ms
     *               = 120 cycles in 1us or 1000ns
     *               = 12 cycles in 100ns
     */

    uint32_t   duty = 500; //500ns
    duty = (500 *12)/100;

    PWM_Params params;

    ClockP_FreqHz freq;
      /*
       * freq = 120Mhz = 120,000,000 cycles in 1s
       *               = 120,000 cycles in 1ms
       *               = 120 cycles in 1us or 1000ns
       */

     ClockP_getCpuFreq(&freq);

     PWM_init();

     params.dutyUnits = PWM_DUTY_COUNTS;  //unit in cpu clock count
     params.dutyValue = duty;
     params.periodUnits = PWM_PERIOD_COUNTS; //unit in cpu clock count
     params.periodValue = pwmPeriod;

     pwm1= PWM_open(Board_PWM1, &params);     //for pulse 1
     pwm3= PWM_open(Board_PWM3, &params);     //for pulser2
     pwm4= PWM_open(Board_PWM4, &params);     //For DAC1 use for pulser1 and pulser2
     pwm7= PWM_open(Board_PWM7, &params);     //For DAC2 reserved
     if ((pwm1 == NULL)||(pwm3 == NULL) ||(pwm4 == NULL)||(pwm7 == NULL)){
           /* Board_PWM0 did not open */
           while (1);
     }


       redoPWM_Init();
       PWM_start(pwm1);
       PWM_start(pwm3);
       PWM_start(pwm4);
       PWM_start(pwm7);

       PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, duty);

       uint32_t ui32Delay = 10000; //ns
       ui32Delay = (ui32Delay * 12)/100;

        PWMPulseWidthSetWithDelay(PWM0_BASE, PWM_OUT_3,
                               duty, ui32Delay);

        //DAC1 and DAC2 output = 0
        duty  = 0;
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, duty);
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_6, duty);


}


void *pwmThread(void *arg0)

{

    //uint16_t   pwmPeriod = 15;      //us, from 15-50 us
    uint32_t   pwmPeriod = 15000;      //in ns
    pwmPeriod = (pwmPeriod *12)/100;    //in cycle
    /*
     * freq = 120Mhz = 120,000,000 cycles in 1s
     *               = 120,000 cycles in 1ms
     *               = 120 cycles in 1us or 1000ns
     *               = 12 cycles in 100ns
     */

    uint32_t   duty = 500; //500ns
    duty = (500 *12)/100;

      PWM_Params params;

      ClockP_FreqHz freq;
      /*
       * freq = 120Mhz = 120,000,000 cycles in 1s
       *               = 120,000 cycles in 1ms
       *               = 120 cycles in 1us or 1000ns
       */

     ClockP_getCpuFreq(&freq);

     PWM_init();

     params.dutyUnits = PWM_DUTY_COUNTS;  // cpu clock count
     params.dutyValue = duty;
     params.periodUnits = PWM_PERIOD_COUNTS; //cpu clock count

     params.periodValue = pwmPeriod;

     pwm1= PWM_open(Board_PWM1, &params);
     pwm3= PWM_open(Board_PWM3, &params);
     if ((pwm1 == NULL)||(pwm3 == NULL)){
           /* Board_PWM0 did not open */
           while (1);
      }

     params.periodValue = 160;
     pwm4= PWM_open(Board_PWM4, &params);

     if (pwm4 == NULL){
            /* Board_PWM0 did not open */
            while (1);
        }

     redoPWM_Init();
     PWM_start(pwm1);
     PWM_start(pwm3);
     PWM_start(pwm4);

     PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, duty);

     uint32_t ui32Delay = 10000; //ns
     ui32Delay = (ui32Delay * 12)/100;

     PWMPulseWidthSetWithDelay(PWM0_BASE, PWM_OUT_3,
                               duty, ui32Delay);

     return (0);
}

