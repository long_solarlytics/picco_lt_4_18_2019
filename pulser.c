/*
 * pulser.c
 *
 *  Created on: Mar 7, 2019
 *      Author: LTran
 */

#include <pulser.h>
#include <ti/drivers/PWM.h>
#include <ti/devices/msp432e4/driverlib/inc/hw_pwm.h>
#include <ti/devices/msp432e4/driverlib/pwm.h>
#include <ti/devices/msp432e4/inc/msp432e411y.h>
#include <ti/devices/msp432e4/driverlib/types.h>


#define PWM_GEN_BADDR(_mod_, _gen_)                                           \
                                ((_mod_) + (_gen_))

#define PWM_OUT_BADDR(_mod_, _out_)                                           \
                                ((_mod_) + ((_out_) & 0xFFFFFFC0))

void PulserControl(pulserControlMode_enum pulserControlMode, uint32_t value) //period = ns
{
    uint32_t presentPeriod, presentShift, presentWidth, presentPwm0CompareB, presentPwm1CompareA;
    uint32_t period, width, shift, pwmEnable;
    uint32_t pwm1CompareA;
    uint32_t pwm0Base = PWM_GEN_BADDR(PWM0_BASE, PWM_GEN_0);
    uint32_t pwm1Base = PWM_GEN_BADDR(PWM0_BASE, PWM_GEN_1);
    uint32_t pwm2Base = PWM_GEN_BADDR(PWM0_BASE, PWM_GEN_2);

    presentPeriod = HWREG(pwm0Base + PWM_O_X_LOAD);

    presentPwm0CompareB = HWREG(pwm0Base + PWM_O_X_CMPB);

    presentWidth = presentPeriod - presentPwm0CompareB;
    presentPwm1CompareA = HWREG(pwm1Base + PWM_O_X_CMPA);

    presentShift = presentPeriod - presentPwm1CompareA;

    pwmEnable = HWREG(PWM0_BASE + PWM_O_ENABLE);

    if (pulserControlMode != PULSER_CTL_VOLTAGE)
    {// = 12 cycles in 100ns
        value = (value * 12)/100;
    }

    switch (pulserControlMode)
    {
        case PULSER_CTL_REPEATRATE:
            period = value;
            HWREG(pwm0Base + PWM_O_X_LOAD) = period;
            HWREG(pwm1Base + PWM_O_X_LOAD) = period;
            HWREG(pwm0Base + PWM_O_X_CMPA) = period - presentWidth;
            HWREG(pwm0Base + PWM_O_X_CMPB) = period - presentWidth;
            pwm1CompareA = period - presentShift;
            HWREG(pwm1Base + PWM_O_X_CMPA) = pwm1CompareA;

            HWREG(pwm1Base + PWM_O_X_CMPB) = pwm1CompareA - presentWidth;
            HWREG(PWM0_BASE + PWM_O_SYNC) =3;         //reset counter pwm 0 and 1
            break;

        case PULSER_CTL_WIDTH:
            width = value;
            HWREG(pwm0Base + PWM_O_X_CMPA) = presentPeriod - width;
            HWREG(pwm0Base + PWM_O_X_CMPB) = presentPeriod - width;

            HWREG(pwm1Base + PWM_O_X_CMPB) = presentPwm1CompareA - width;
            break;
        case PULSER_CTL_PHASEDELAY:
              shift = value;
              pwm1CompareA =  presentPeriod - shift;
              HWREG(pwm1Base + PWM_O_X_CMPA) = pwm1CompareA;

              HWREG(pwm1Base + PWM_O_X_CMPB) = pwm1CompareA - presentWidth;

              break;

        case PULSER_CTL_VOLTAGE:
            /*
             * value = voltage in volt
             * According to John: voltage ranges from 30V = .588, to 150V = 3V  => 1V ~ .02V
             * Full voltage of DAC = 3.3V for amplitude = 3.3/.02 = 165V
             */
            HWREG(pwm2Base + PWM_O_X_LOAD) = 166;
            presentPeriod =HWREG(pwm2Base + PWM_O_X_LOAD);
            HWREG(pwm2Base + PWM_O_X_CMPA) = 166 - value;
            presentPeriod = HWREG(pwm2Base + PWM_O_X_CMPA);
            HWREG(pwm2Base + PWM_O_ENABLE) = pwmEnable | PWM_ENABLE_PWM4EN;
            break;

        case PULSER_CTL_START:
              HWREG(PWM0_BASE + PWM_O_ENABLE) = pwmEnable | PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM3EN;
             break;

        case PULSER_CTL_END:
            HWREG(PWM0_BASE + PWM_O_ENABLE) = pwmEnable & ~PWM_ENABLE_PWM1EN & ~PWM_ENABLE_PWM3EN;
             break;


        default:
            break;

    }
}



