/*
 * piccolo.h
 *
 *  This module contains global declarations used throughout
 *  the application
 *
 *  Created on: Dec 1, 2018
 *      Author: tjc2c
 */

#ifndef PICCOLO_H_
#define PICCOLO_H_


/* Stack size in bytes */
#define HANDLERSTACKSIZE         2048

/* Default sleep time in seconds between status message requests */
#define DEF_STATUS_SLEEP_TIME_SECS 20

/* Default JSON buffer size */
#define DATA_SIZE 1024

/* HTTP request/response message codes */
#define MSG_CODE_STATUS_REG     1
#define MSG_CODE_ACKNOWLEDGE    2
#define MSG_CODE_STATUS_OG      3
#define MSG_CODE_CMD_RESPONSE   4

/* Time to wait between network connection attempts */
#define CONNECT_SLEEP_TIME_SECS 10

/* Thread mutex for runThreads global */
pthread_mutex_t threadLock;
/* Checked by all running threads, terminates them if set to 0 */
volatile int runThreads;

/* Global configuration parameters data structure */
typedef struct configParams_ {
    char deviceId[17];
    char macAddress[13];
    char siteId[24];
    char sensorId[65];
    int  statusInterval;
    unsigned long messageCnt;
} configParams;

/* Global device configuration parameters */
configParams deviceConfigParams;

/* Gobal terminal display i/o handler */
#include <ti/display/Display.h>
Display_Handle display;


#endif /* PICCOLO_H_ */
