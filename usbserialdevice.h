/*
 * usbserialdevice.h
 *
 *  Created on: Mar 12, 2019
 *      Author: LTran
 */

#ifndef USBSERIALDEVICE_H_
#define USBSERIALDEVICE_H_

#include <stdint.h>

void *transmitFxn(char *arg0);
void *receiveFxn(void *arg0);

typedef struct
{
    unsigned char buf[32];
    uint32_t index;
}usbRxBuf_t;



#endif /* USBSERIALDEVICE_H_ */
