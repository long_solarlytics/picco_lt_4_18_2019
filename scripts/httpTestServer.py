#!/usr/bin/env python3
"""
Very simple HTTP server in python for testing requests from the MCU
Usage::
    ./httpTestServer.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json
from logging.handlers import _MIDNIGHT
import builtins

class S(BaseHTTPRequestHandler):
    
    def buildAckMsg(self,reqData,cmdType,interval):
        respJson = {}
        respJson['code'] = 3
        respJson['format'] = 0
        respJson['mac'] = reqData['mac']
        respJson['iotMessageId'] = reqData['iotMessageId']
        respJson['webMessageId'] = 1
        if cmdType == "setup":
            respJson['command'] = self.buildSetupCmd()
        elif cmdType == "query":
            respJson['command'] = self.buildQueryCmd()
        elif cmdType == "measure":
            respJson['command'] = self.buildMeasureCmd()
        return respJson
    
    def buildSetupCmd(self):
        command = {}
        command['type'] = 'setup'
        command['statusInterval'] = 20
        command['pulseMode'] = 'default'
        command['pulseMode'] = 'pulsePlan'
        
        pulseDefaults = {}
        pulseDefaults['pulseWidth'] = 250
        pulseDefaults['pri'] = 25
        pulseDefaults['phaseDelay'] = 1000
        pulseDefaults['enable'] = True
        command['defaults'] = pulseDefaults
        pulsePlanArray = []

        pulsePlanItem = {}
        pulsePlanItem['id'] = 1
        pulsePlanItem['startTime'] = '00:00:00'
        pulsePlanItem['duration'] =  79200
        pulsePlanItem['pulseWidth'] = 250
        pulsePlanItem['pri'] = 15
        pulsePlanItem['phaseDelay'] = 1000
        pulsePlanItem['enable'] = True
        pulsePlanItem['delete'] = False
        pulsePlanArray.append(pulsePlanItem);

        pulsePlanItem = {}
        pulsePlanItem['id'] = 2
        pulsePlanItem['startTime'] = '22:00:00'
        pulsePlanItem['duration'] =  7200
        pulsePlanItem['pulseWidth'] = 500
        pulsePlanItem['pri'] = 30
        pulsePlanItem['phaseDelay'] = 2000
        pulsePlanItem['enable'] = True
        pulsePlanItem['delete'] = False
        pulsePlanArray.append(pulsePlanItem);

        command['pulsePlan'] = pulsePlanArray
        manualMode = {}
        manualMode['pulseWidth'] = 500
        manualMode['pri'] = 50
        manualMode['phaseDelay'] = 7500
        manualMode['enable'] = True
        command['manualMode'] = manualMode
        tempAlert = {}
        tempAlert['ambient'] = 50
        tempAlert['pulser'] = 100
        tempAlert['powerSupply'] = 200
        command['tempAlert'] = tempAlert
        voltageAlert = {}
        voltageAlert['low'] = 10
        voltageAlert['high'] = 250
        command['voltageAlert'] = voltageAlert
        currentAlert = {}
        currentAlert['low'] = 20
        currentAlert['high'] = 100
        command['currentAlert'] = currentAlert
        powerAlert = {}
        powerAlert['low'] = 25
        powerAlert['high'] = 300
        command['powerAlert'] = powerAlert
        suspendOps = {}
        suspendOps['temp'] = 1000
        suspendOps['current'] = 15
        command['suspendOps'] = suspendOps
        return command
    
    
    def buildQueryCmd(self):
        command = {}
        command['type'] = 'query'
        command['statusInterval'] = 60 
        command['pulsePlanId'] = 1
        return command
 
    def buildMeasureCmd(self):
        command = {}
        command['type'] = 'measure'
        command['statusInterval'] = 60
        return command
   
    def _set_response(self,respBytes):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Content-length',len(respBytes))
        self.end_headers()

    def do_GET(self):
        self.do_POST();
    
    def handleRegistrationRequest(self,post_data):
        logging.info("In handle registration request")
        postDataStr = post_data.decode("utf-8")
        postDataObj = json.loads(postDataStr)
        statusData = postDataObj['statusLog']
        logging.info("Post Data status = %s" %(statusData))
        respJson = self.buildAckMsg(statusData,"setup",1)
                     
        respBytes = bytes(json.dumps(respJson), "utf-8")
        
        self._set_response(respBytes)
        self.wfile.write(respBytes)
        
        
    def handleStatusRequest(self,post_data):
        logging.info("In handle status request")
        postDataStr = post_data.decode("utf-8")
        postDataObj = json.loads(postDataStr)
        statusData = postDataObj['statusLog']
        logging.info("Post Data status = %s" %(statusData))
        
        mid = statusData['iotMessageId']
        interval = mid%2
        if interval == 0:
           respJson = self.buildAckMsg(statusData,"measure",interval)
        elif interval == 1:
           respJson = self.buildAckMsg(statusData,"query",interval) 
                     
        respBytes = bytes(json.dumps(respJson), "utf-8")
        
        self._set_response(respBytes)
        self.wfile.write(respBytes)

    def handleCommandRequest(self,post_data):
        logging.info("In handle command request")
        postDataStr = post_data.decode("utf-8")
        postDataObj = json.loads(postDataStr)
        commandData = postDataObj['commandData']
        logging.info("Post Data status = %s" %(commandData))
        # Process any incoming data from the command request then send a response
        respJson = self.buildAckMsg(commandData,"measure",1)         
        respBytes = bytes(json.dumps(respJson), "utf-8")
        
        self._set_response(respBytes)
        self.wfile.write(respBytes)
        

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path), str(self.headers), post_data.decode('utf-8'))

        if str(self.path) == "/status":
            self.handleStatusRequest(post_data)
        elif str(self.path) == "/registration":
            self.handleRegistrationRequest(post_data)
        else:
            self.handleCommandRequest(post_data)

#        logStatus(post_data)
        


def logStatus(statusData):
    statusData = statusData.decode('utf-8')
    r = requests.get('http://localhast:5000', data={'statusLog': statusData})
    print(r.status_code, r.reason)


def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv
    
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
