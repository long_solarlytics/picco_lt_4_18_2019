#! python

import threading
import socket
import getopt
import sys
import queue
import time

      
class commandThread (threading.Thread):
   def __init__(self, threadID, name,port,q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.port = port
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      commandServer(self.name,self.port,self.q)
      print ("Exiting " + self.name)

      
def sendCommand(serverSocket,command):
    try:
        print("sendCommand: Sending command: %s" %(command));
        serverSocket.sendall(bytes(command,'utf-8'))
        data = serverSocket.recv(3)
        dataStr = str(data,'utf-8')
        print("sendCommand:  Received msg from server: %s, len = %d" %(dataStr,len(dataStr)));
    except socket.error as msg:
        print("sendCommand:  error returned " + msg)

        
def commandServer(name,port,q):
    print ("commandServer: Starting thread %s" %(name))
    BUFFSIZE = 1024
    HOST = ''               # Symbolic name meaning the local host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, port))
    s.listen(1)
    
    conn, addr = s.accept()
    sendCommand(conn,str(3));
    conn.close()
    
    buffer = bytearray(BUFFSIZE)
    cmd = 1
    while q.empty():
        print ("commandServer: Waiting for incoming client connection")
        conn, addr = s.accept()
        print ("commandServer: socket address ip = %s, port = %d" %(addr[0],addr[1]))
        sendCommand(conn,str(cmd));
        if cmd == 1:
            cmd = 2
        else:
            cmd = 1
        time.sleep(10)
        conn.close()
        
#        ans=True
#        while ans:
#            print("""
#            1. Toggle LED 0
#            2. Take ADC Measurement
#            3. Run PWM (ramp LED 1)
#            4. Exit Program
#            """)
#            ans=input("What would you like to do? ")
#            if ans=="1":
#              sendCommand(conn,ans)
#            elif ans=="2":
#              sendCommand(conn,ans)
#            elif ans=="3":
#              sendCommand(conn,ans)
#            elif ans=="4":
#              q.put(True);
#              break
#            else:
#               print("\n Not Valid Choice Try again")
               
#        if ans == "4":
#            break;
        
    print ("commandServer: Terminating thread %s" %(name))
    
   


q = queue.Queue()

cmdThread = commandThread(1,"Command Thread",1001,q)
cmdThread.start()

cmdThread.join()

print("Done")

sys.exit(1)