#! python

import threading
import socket
import getopt
import sys
import queue

class commandThread (threading.Thread):
   def __init__(self, threadID, name,port,q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.port = port
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      commandServer(self.name,self.port,self.q)
      print ("Exiting " + self.name)
      
class statusThread (threading.Thread):
   def __init__(self, threadID, name,q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      statusServer(self.name,self.q)
      print ("Exiting " + self.name)

class receiverThread (threading.Thread):
   def __init__(self, threadID, name,port,q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.port = port
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      dataReceiver(self.name,self.port,self.q)
      print ("Exiting " + self.name)
      
def sendCommand(serverSocket,command):
    try:
        print("sendCommand: Sending command: %s" %(command));
        serverSocket.sendall(bytes(command,'utf-8'))
        data = serverSocket.recv(2)
        dataStr = str(data,'utf-8')
        print("sendCommand:  Received msg from server: %s, len = %d" %(dataStr,len(dataStr)));
    except socket.error as msg:
        print("sendCommand:  error returned " + msg)

def dataReceiver(name,port,q):
    print ("dataReceiver: Starting thread %s" %(name))
    BUFFSIZE = 1024
    HOST = ''               # Symbolic name meaning the local host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, port))
    s.listen(1)

    print ("dataReceiver: Waiting for incoming client connection")
    conn, addr = s.accept()
    print ("dataReceiver: socket address ip = %s, port = %d" %(addr[0],addr[1]))

    buffer = bytearray(BUFFSIZE)
    while q.empty():
        data = conn.recv(256)
        dataStr = str(data,'utf-8')
        print("dataReceiver: Data received: %s, len = %d" %(dataStr,len(dataStr)));
        conn.sendall(bytes("OK",'utf-8'))
        
    print ("dataReceiver: Terminating thread %s" %(name))
    
    conn.close()

        
def commandServer(name,port,q):
    print ("commandServer: Starting thread %s" %(name))
    BUFFSIZE = 1024
    HOST = ''               # Symbolic name meaning the local host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, port))
    s.listen(1)
    
    print ("commandServer: Waiting for incoming client connection")
    conn, addr = s.accept()
    print ("commandServer: socket address ip = %s, port = %d" %(addr[0],addr[1]))
    # Start a data receiver thread
    recvrThread = receiverThread(3,"Receiver Thread",1002,q)
    recvrThread.start()
    
    # Start a command client
    
    buffer = bytearray(BUFFSIZE)
    while q.empty():
        ans=True
        while ans:
            print("""
            1. Toggle LED 0
            2. Take ADC Measurement
            3. Run PWM (ramp LED 1)
            4. Exit Program
            """)
            ans=input("What would you like to do? ")
            if ans=="1":
              sendCommand(conn,ans)
            elif ans=="2":
              sendCommand(conn,ans)
            elif ans=="3":
              sendCommand(conn,ans)
            elif ans=="4":
              q.put(True);
              break
            else:
               print("\n Not Valid Choice Try again")
               
        if ans == "4":
            break;
        
    print ("commandServer: Terminating thread %s" %(name))
    
    conn.close()


def statusServer(name,q):
    print ("statusServer: Starting thread %s\n" %(name))
    HOST = ''                # Symbolic name meaning the local host
    PORT = 1000              # status port
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    global exitAll
    
    commThread = commandThread(2,"Command Thread",1001,q)
    
    while q.empty():
        print ("statusServer: Waiting for incoming client connection")
        conn, addr = s.accept()
        print ("statusServer: socket address ip = %s, port = %d" %(addr[0],addr[1]))
    
        data = conn.recv(1024)
        print("statusServer: Received client registration message: %s\n" %(str(data,'utf-8')))
        
        commThread.start()
        conn.sendall(bytes(str(1001),'utf-8')) 
        
        while q.empty():
            print("statusServer: Waiting for next status message")
            data = conn.recv(1024)
            if not data:
               print("statusServer: Client connection appears to be closed, will wait for new connection")
               break
            print("Received status info: %s\n" %(str(data,'utf-8')))

    
    print ("statusServer: Terminating thread %s" %(name))
    
    conn.close()

q = queue.Queue()

statThread = statusThread(1,"Status Thread",q)
statThread.start()

statThread.join()

print("Done")

sys.exit(1)