#! python

import threading
import socket
import getopt
import sys
import queue

      
class statusThread (threading.Thread):
   def __init__(self, threadID, name,q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      statusServer(self.name,self.q)
      print ("Exiting " + self.name)



def statusServer(name,q):
    print ("statusServer: Starting thread %s\n" %(name))
    HOST = ''                # Symbolic name meaning the local host
    PORT = 1000              # status port
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    global exitAll
        
    while q.empty():
        print ("statusServer: Waiting for incoming client connection")
        conn, addr = s.accept()
        print ("statusServer: socket address ip = %s, port = %d" %(addr[0],addr[1]))
    
        data = conn.recv(256)
        print("statusServer: Received client registration message: %s\n" %(str(data,'utf-8')))
        
        conn.sendall(bytes("ACK",'utf-8'))
                
        while q.empty():

            print("statusServer: Waiting for next status message")
            data = conn.recv(256)
            if not data:
               print("statusServer: Client connection appears to be closed, will wait for new connection")
               break
            
            print("Received status info: %s\n" %(str(data,'utf-8')))
            conn.sendall(bytes("ACK",'utf-8'))

    
    print ("statusServer: Terminating thread %s" %(name))
    
    conn.close()

q = queue.Queue()

statThread = statusThread(1,"Status Thread",q)
statThread.start()

statThread.join()

print("Done")

sys.exit(1)