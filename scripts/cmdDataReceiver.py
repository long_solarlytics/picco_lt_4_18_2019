#! python

import threading
import socket
import getopt
import sys
import queue


class receiverThread (threading.Thread):
   def __init__(self, threadID, name,port,q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.port = port
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      dataReceiver(self.name,self.port,self.q)
      print ("Exiting " + self.name)


def dataReceiver(name,port,q):
    print ("dataReceiver: Starting thread %s" %(name))
    BUFFSIZE = 1024
    HOST = ''               # Symbolic name meaning the local host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, port))
    s.listen(1)


    buffer = bytearray(BUFFSIZE)
    while q.empty():
        print ("dataReceiver: Waiting for incoming client connection")
        conn, addr = s.accept()
        print ("dataReceiver: socket address ip = %s, port = %d" %(addr[0],addr[1]))

        data = conn.recv(256)
        dataStr = str(data,'utf-8')
        print("dataReceiver: Data received: %s, len = %d" %(dataStr,len(dataStr)));
        conn.sendall(bytes("ACK",'utf-8'))
        conn.close()
        
    print ("dataReceiver: Terminating thread %s" %(name))
    
    conn.close()



q = queue.Queue()

cmdThread = receiverThread(1,"Command Rx Thread",1002,q)
cmdThread.start()

cmdThread.join()

print("Done")

sys.exit(1)