/*
 * parseCmd.c
 *
 *  Created on: Mar 4, 2019
 *      Author: LTran
 */





/*===================================================================*/
/*                                                                   */
/* Project    : A380 ACP Hardware Test (MAXI)                        */
/*                                                                   */
/* Module Name: ACP_PARS.C                                           */
/*                                                                   */
/* Requirement:                                                      */
/*                                                                   */
/* Description:                                                      */
/*                                                                   */
/* History:                                                          */
/* 29.04.03 0.0 First issue                                          */
/*                                                                   */
/*===================================================================*/

//#include <reg167cs.h>
#include <ti/sysbios/knl/Swi.h>

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include "timer.h"
#include "parseCmd.h"
#include "pwm.h"
#include "pulser.h"
#include "wifi.h"

#include <ti/drivers/UART.h>

//////////////#include "acp_sio.h"
////////////#include "acp_tio.h"
///////////////#include "acp_rti.h"
///////////////#include "acp_exec.h"

extern Timer_Handle timer1;
cmd_token
scanner( char *cmds )
{
    cmd_token cmd = UNKNOWN;

    switch( cmds[0] )
    {
        case 'c':
        case 'C':
          cmd = COMMANDS;

          break;

        case 'p':
        case 'P':
            cmd = PULSER_CONTROL;

            break;

        case 'w':
        case 'W':
             cmd = WIFI_CONTROL;

             break;
        default:
            break;

    } /* end switch */

    return cmd;
}


/*-------------------------------------------------------------------*/
/*                                                                   */
/* Function Name: stoul                                              */
/*                                                                   */
/* Funct. Description:                                               */
/*    This function converts a numeric string (hex or decimal)       */
/*    to the corresponding unsigned long value.                      */
/*                                                                   */
/* Design Description:                                               */
/*                                                                   */
/*-------------------------------------------------------------------*/
uint32_t
//stoul( char *s, arg_base b )
stoul( char *s)
{
    uint32_t f = 1;            /* multiplication factor */
    unsigned char c;                /* coeficient */
    uint32_t n = 0;            /* number */
    int i, len;

    len = strlen( s ) - 1;          /* point to last char */
    for ( i = len-1; i >= 0; i-- )
    //for ( i = 0; i < len; i++ )
    {
 //       temp = s[i];
        c = s[i] -'0';
        n = n + c * f;
        f = f * 10;
    }
    return( n );
}
#if 0
static void itoa(uint32_t n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
         s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}
#endif

extern Swi_Handle swi0Handle;

//extern UART_Handle uart0;
///extern UART_Handle uart6;

const char helpPrompt__[]       = "1234567890123456789Valid Commands\r\n"                  \
                                "--------------\r\n"                  \
                                "h: help\r\n"                         \
                                "q: quit and shutdown UART\r\n"       \
                                "c: clear the screen\r\n"             \
                                "t: display current temperature\r\n";
/*-------------------------------------------------------------------*/
/*                                                                   */
/* Function Name: perform_cmd                                        */
/*                                                                   */
/* Funct. Description:                                               */
/*    Perform command                                                */
/*                                                                   */
/* Design Description:                                               */
/*                                                                   */
/*-------------------------------------------------------------------*/
void
//perform_cmd( cmd_token cmd_t, char *cmds )
perform_cmd( cmd_token cmd_t, char *cmds )
{
    uint32_t value;
    pulserControlMode_enum pulserControlMode;
    value = stoul( &cmds[2]);
   // = 12 cycles in 100ns
  //////  value = (value * 12)/100;
#if 0
    const char __helpPrompt[]       = "1234567890123456789Valid Commands\r\n"                  \
                                                        "--------------\r\n"                  \
                                                        "h: help\r\n"                         \
                                                        "q: quit and shutdown UART\r\n"       \
                                                        "c: clear the screen\r\n"             \
                                                        "t: display current temperature\r\n";

#endif
    switch( cmd_t ) {
        case COMMANDS:
            switch(cmds[1]){
                case 'h':

       ////////////              UART_write(uart0, __helpPrompt, sizeof(__helpPrompt));
                    break;
            }
            break;
        case PULSER_CONTROL:

            switch(cmds[1]){

                case 'r':
                    pulserControlMode = PULSER_CTL_REPEATRATE;
                    break;
                case 'w':
                     pulserControlMode = PULSER_CTL_WIDTH;
                     break;
                case 'd':
                    pulserControlMode = PULSER_CTL_PHASEDELAY;
                    break;
                case 'v':
                    pulserControlMode = PULSER_CTL_VOLTAGE;
                    break;
                case 's':
                     pulserControlMode = PULSER_CTL_START;
                     break;
                case 'e':
                      pulserControlMode = PULSER_CTL_END;
                      break;
                default:
                    break;
            }
            PulserControl(pulserControlMode, value); //period = ns
            break;

       case WIFI_CONTROL:

           switch(cmds[1]){
               case 'd':
               //    Swi_post( swi0Handle);//wifiDetect();
                   //wifiDetect();
     /////////////////////              UART_write(uart6, __helpPrompt, sizeof(__helpPrompt));
                   break;

               default:
                   break;
           }
           break;

       default:
            break;
    }
}




