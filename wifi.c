
#if 0
//this file is reserved for the function: embedded programming the CC3120 via srt of the MSP432E



/*
 * wifi.c
 *
 *  Created on: Mar 22, 2019
 *      Author: LTran
 */
#include <stdint.h>
#include "timer.h"
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>
#include "usbserialdevice.h"
#include "MSP_EXP432E401Y.h"
#include "delayTime.h"
#include <stdbool.h>
#include <string.h>

#include <ti/devices/msp432e4/inc/msp432e411y.h>
//#include <ti/drivers/uart/UARTMSP432E4.h>

#include "uart.h"

#define UART6_BASE                      ((uint32_t)0x40012000)
extern void UARTBreakCtl(uint32_t ui32Base, bool bBreakState);

/*
 * The following function is from good old K & R.
 */
#if 0
static void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itoa(int n, char s[]);
/*
 * The following function is from good old K & R.
 */
static void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
         s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}
#endif
#if 0
/*-------------------------------------------------------------------*/
/*                                                                   */
/* Function Name: uctoa                                              */
/*                                                                   */
/* Funct. Description:                                               */
/*    Converts a byte value (unsigned char) to two ASCII hex         */
/*    characters.                                                    */
/*                                                                   */
/* Design Description:                                               */
/*                                                                   */
/*-------------------------------------------------------------------*/
const char hexchar[16] = { '0','1','2','3','4','5','6','7',
                       '8','9','A','B','C','D','E','F' };

void
uctoa( char *s, unsigned char n )
{
 //   const char hexchar[16] = { '0','1','2','3','4','5','6','7',
 //                          '8','9','A','B','C','D','E','F' };
    uint32_t i=0;
  //  *s++ = hexchar[ n >> 4 ];
  //  *s++ = hexchar[ n & 0x0F ];
  //  *s   = '\0';
    n= n >> 4;
     s[i] = hexchar[ n];
     i= i + 1;
     n= n & 0x0F;
     //s[i++] = hexchar[ n & 0x0F ];
     s[i] = hexchar[ n];
   s[i]    = '\0';
}

#endif

#if 1
/*-------------------------------------------------------------------*/
/*                                                                   */
/* Function Name: uctoa                                              */
/*                                                                   */
/* Funct. Description:                                               */
/*    Converts a byte value (unsigned char) to two ASCII hex         */
/*    characters.                                                    */
/*                                                                   */
/* Design Description:                                               */
/*                                                                   */
/*-------------------------------------------------------------------*/

void
uctoa( char *s, unsigned char n )
{
    const char hexchar[16] = { '0','1','2','3','4','5','6','7',
                           '8','9','A','B','C','D','E','F' };
  //  uint32_t i=0;
    *s++ = hexchar[ n >> 4 ];
    *s++ = hexchar[ n & 0x0F ];
  //  *s   = '\0';
   // n= n >> 4;
   //  s[i] = hexchar[ n];
   //  i= i + 1;
   //  n= n & 0x0F;
     //s[i++] = hexchar[ n & 0x0F ];
   //  s[i] = hexchar[ n];
   *s    = '\0';
}

#endif

/* POSIX Header files */
#include <pthread.h>
#include <semaphore.h>
sem_t semWifi;

volatile bool uart6Enabled = true;
sem_t semUart6;

//extern UART_Handle uart0, uart7;
//void wifiDetect(void)
void wifiDetect(UART_Handle uart)
{
    //unsigned char        input[2];
    uint32_t        numBytes;
    unsigned char data[32];
  //  char cmd, cmds[80];
  //  uint32_t i;
    UART_Params uartParams;
//     UART_Handle uart6;
    while (1){
#if 0
     /* Create a UART with data processing off. */
      UART_Params_init(&uartParams);
      uartParams.writeDataMode = UART_DATA_BINARY;
      uartParams.readDataMode = UART_DATA_BINARY;
      uartParams.readReturnMode = UART_RETURN_FULL;
      uartParams.readEcho = UART_ECHO_OFF;
   //////////   uartParams.baudRate = 921600;
   ///   uartParams.readMode = UART_MODE_CALLBACK;
   ///   uartParams.writeMode = UART_MODE_CALLBACK;

     //// uart6 = UART_open(MSP_EXP432E401Y_UART6, &uartParams);
      uart6 = UART_open(Board_UART0, &uartParams);

      if (uart6 == NULL) {
          /* UART_open() failed */
          while (1);
      }
#endif
      delayActivate(Timer_PERIOD_US, 1500000);  //delay 1.5 ms
  //    while (1) {
 //         UART_read(uart, &input, 1);
  //        UART_write(uart, &input, 1);
  //    }
/////////      UART_read(uart, &input, 16);
#if 0
      while (UARTCharsAvail(UART6_BASE))

      {
          UART_readPolling(uart, data, 1);
      }
#endif

  /////////////////////////////////////////////////////    uart_flush(uart6);
  //    UART_readPolling(uart, input, 32);
 //   GPIO_write( MSP_EXP432E401Y_nHIB_pin, 1);
    GPIO_write( MSP_EXP432E401Y_nHIB_pin, 0);
 //   GPIO_write( MSP_EXP432E401Y_WIFI_RX, 1);
    delayActivate(Timer_PERIOD_US, 1500);  //delay 1.5 ms
   // delayActivate(Timer_PERIOD_US, 1500000);  //delay 1.5 ms
 ////////////////////////////////////////////////////////////////////////////   UARTBreakCtl(UART6_BASE, true);
//    GPIO_write( MSP_EXP432E401Y_WIFI_RX, 0);

 /////////////////////////////////////   delayActivate(Timer_PERIOD_US, 1500);  //delay 1.5 ms
    GPIO_write( MSP_EXP432E401Y_nHIB_pin, 1);
  //  delayActivate(Timer_PERIOD_US, 150000);  //delay 100 ms
#if 0
    i=0;
    do {       /* generate digits in reverse order */
         //status = UART_read(uart, &cmd, sizeof(cmd));
         //UART_write(uart, &cmd, sizeof(cmd));
      //  cmd=  receiveFxn(data);
        //UART_read(uart, &input, 1);
        input[i++]= UARTCharGet(UART6_BASE);
      //  input[i++] = 0xCC;
        //if (received) {
//              cmd[0] = data[0];
        //     input[1] = '\0';
        //     transmitFxn((char *)input);
        //     transmitFxn((char *)("\n>\0"));  //enter newline and prompt >
      //  }
     //   if ((input[0] == 0xCC) ||(input[0] == 0xcc))
      //  {
     //       i= i++;
      //  }

     } while (i<=2);//(input[0] != 0xCC);  //0x0D = 'CR'
     transmitFxn((char *)input);
     transmitFxn((char *)("\n>\0"));  //enter newline and prompt >


#endif
   //  UARTMSP432E4_read(UART_Handle handle, void *buffer, size_t size);
   //  UART_read(UART_Handle handle, void *buffer, size_t size);
#if 0
     //char *s;
     char s[3];
 //    uint32_t num= UART_read(uart, data, 2);
     data[0] = 0xAB;
     data[1] = 0xCC;
     uctoa( s, data[0] );

     transmitFxn((char *)s);
     uctoa( s, data[1] );

      transmitFxn((char *)s);

     transmitFxn((char *)("\n>\0"));  //enter newline and prompt >
#endif
 //////////////////////////////////   delayActivate(Timer_PERIOD_US, 50000);  //delay 50 ms
///////////////////////////////////////////////////////////////    UARTBreakCtl(UART6_BASE, false);
////////////////////////////    delayActivate(Timer_PERIOD_US, 1500);  //delay 1.5 mslongT
 //   data[0] = 0xAB;
 //   data[1] = 0xCC;


    const char __helpPrompt[]       = "1234567890123456789Valid Commands\r\n"                  \
                                    "--------------\r\n"                  \
                                    "h: help\r\n"                         \
                                    "q: quit and shutdown UART\r\n"       \
                                    "c: clear the screen\r\n"             \
                                    "t: display current temperature\r\n";
 //   numBytes =  UART_write(uart0, &data, 4);
    UART_write(uart, __helpPrompt, sizeof(__helpPrompt));

  //  UART_write(uart0, __consoleDisplay, sizeof(__consoleDisplay));
  //  numBytes =  UART_write(uart6, consoleDisplay, 2);


#if 0
    UART_read(uart, &input, 2);
    input[2] =  '\0';
    transmitFxn((char *)input);
#endif

 ///////////////////   delayActivate(Timer_PERIOD_US, 50000);  //delay 50 ms
    ///UART_close(uart6);
    ////////////////////UART_close(uart0);
    }

}


#endif
