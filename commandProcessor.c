/*
 * commandProcessor.c
 *
 *  Created on: Nov 21, 2018
 *      Author: Tim Carbo
 * Description: This module contains functions that implement
 *              the command processing component of the MCU
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stddef.h>
#include <pthread.h>
#include <mqueue.h>

#include <ti/display/Display.h>
#include <ti/drivers/GPIO.h>
//#include <ti/drivers/PWM.h>
#include <ti/drivers/utils/List.h>
#include <ti/drivers/Timer.h>
#include <ti/utils/json/json.h>

/* Board Header file */
#include "Board.h"

#include "json/tiny-json.h"
#include "json/json-maker.h"

#include "piccolo.h"
#include "commandProcessor.h"
#include "thread_utils.h"
#include "httpClient.h"


int timerCount;
bool timerRunning;


/**
 * These declarations are required by RTOS for
 * threaded processing
 */
extern void fdOpenSession();
extern void fdCloseSession();
extern void *TaskSelf();

bool loadPulseProperties(const json_t *pulseProps,PulseParams *pParams) ;
void loadTempAlertProperties(const json_t *tempJson);
void loadAlertParams(const json_t *alertParams,AlertThresholds *thresholds);
void loadSuspensOpsParams(const json_t *suspendOpsJson);
char *processSetupCommand(const json_t *commandObj,char *resultsJson);
char *processQueryCommand(const json_t *commandObj,char *resultsJson);
char *addMeasurements(char *resultsJson);
char *processMeasureCommand(const json_t *commandObj,char *resultsJson);
void *pwmThread(void *arg0);
void addErrorMessage(const json_t *jsonObj,char *tag,char *msg);
int  getPulsePlanListSize(List_List *list);
void *pulsePlanTimerThread(void *arg0);
void stopPulsePlanTimer();
void startPulsePlanTimer(int duration);


//static Timer_Handle pulsePlanTimer;

/**
 * This function returns a random
 * value using seed parameter as a base
 *
 *  @param seed - seed value
 *  @return seeded random value
 *
 */
double getRandMeasure(double seed) {
    double scale = (double)rand()/32768.0;
    double retVal = seed + seed*scale;
    return retVal;
}


/**
 * Retrieves the panel current, voltage and power
 */
void getPanelMeasurements() {
    panelMeasurements.panelCurrent = getRandMeasure(10.0);
    panelMeasurements.panelVoltage = getRandMeasure(150.0);
    panelMeasurements.panelPower = getRandMeasure(10000.0);
}

/**
 * Pushes an error message onto the circular message store
 *       @param jsonObj - input JSON command object that caused the error
 *       @param tag        - input tag identifier
 *       @param msg        - input message string
 */
void addErrorMessage(const json_t *jsonObj,char *tag,char *msg) {
    //Roll the message buffer over if errorMsgCnt = MAX_ERROR_MSGS
    errorMsgCnt = errorMsgCnt%MAX_ERROR_MSGS;
    sprintf(errorMessages[errorMsgCnt++],"%s: %s",tag,msg);
}


/**
 * Processes a PWM command
 */
bool processPWM(const PulseParams *pulseParms) {

    Display_printf(display, 0, 0, "processPWM:  Begin\n");

 /////////LongT   Timer_runTimersAsPWMWithDelay(pulseParms->width,pulseParms->pri,pulseParms->phaseDelay);

    Display_printf(display, 0, 0, "processPWM:  Done\n");

    return true;
}

/**
 * Parses a pulse plan json object as stores the properties
 * into a PulsePlan structure
 *
 *    @param pp - PulsePlan structure
 *    @param pulsePlanObj - pulse plan JSON object
 *
 *    @return true if successful
 */
bool loadPulsePlanObject(PulsePlan *pp,json_t *pulsePlanObj) {
    int hh,mm,ss;
    const json_t *prop = json_getProperty( pulsePlanObj, "id" );
    if ( !prop || JSON_INTEGER != json_getType( prop ) ) {
        addErrorMessage(pulsePlanObj,"loadPulsePlanProperties","id not a valid integer");
        return false;
    } else {
        pp->id = (int)json_getInteger( prop );

        prop = json_getProperty( pulsePlanObj, "startTime" );
        if ( !prop || JSON_TEXT != json_getType( prop ) ) {
            addErrorMessage(pulsePlanObj,"loadPulsePlanProperties","startTime not a valid string");
            return false;
        } else {
            sscanf(json_getValue( prop ),"%02d:%02d:%02d",&hh,&mm,&ss);

            // Set the start time for this pulse plan
            pp->startTime = hh*3600 + mm*60 + ss;

            prop = json_getProperty( pulsePlanObj, "duration" );
            if ( !prop || JSON_INTEGER != json_getType( prop ) ) {
                addErrorMessage(pulsePlanObj,"loadPulsePlanProperties","duration not a valid integer");
                return false;
            } else {

                // set the duration for this pulse plan
                // This will be used by the timer as a count down
                // until the next pulse plan starts

                pp->duration = json_getInteger( prop );

                prop = json_getProperty( pulsePlanObj, "enable" );
                if ( !prop || JSON_BOOLEAN != json_getType( prop ) ) {
                    addErrorMessage(pulsePlanObj,"loadPulsePlanProperties","enable not a valid boolean");
                } else {
                    pp->enabled = json_getBoolean(prop);
                }

                if ( !loadPulseProperties(pulsePlanObj,&(pp->pulseParams)) ) {
                    return false;
                }
            }
        }
    }
    return true;

}

/**
 * Parses a pulse plan array of JSON objects, creates PulsePlan data and
 * stores them in the PulsePlan linked list
 *
 *    @param pulsePlanArray - array of pulse plan Json objects
 *
 *    @return true if successful
 *
 */
bool loadPulsePlanProperties(const json_t *pulsePlanArray) {

    PulsePlan pp;
    json_t *pulsePlanObj = NULL;
    PulsePlanListElem *pPlan = NULL;
    int count = 0;

    // Count the number of pulse plan objects in the json array.  Make sure we have at least one item.
    for (pulsePlanObj = (json_t *)json_getChild(pulsePlanArray); pulsePlanObj != NULL; pulsePlanObj = (json_t *)json_getSibling(pulsePlanObj)) {
        count++;
    }

    if (count > 0) {
        // We have more than one item so clear the previous list

         for(pPlan = (PulsePlanListElem *)List_head(&pulsePlanList);pPlan != NULL;pPlan = (PulsePlanListElem *)List_next((List_Elem *)pPlan)) {
            if ( pPlan ) {
                free(pPlan);
            }

        }
        List_clearList(&pulsePlanList);

        // Iterate through the pulse plan JSON array, parse each object and insert into the pulse plan
        // linked list

        for (pulsePlanObj = (json_t *)json_getChild(pulsePlanArray); pulsePlanObj != NULL; pulsePlanObj = (json_t *)json_getSibling(pulsePlanObj)) {

            if ( loadPulsePlanObject(&pp,pulsePlanObj) ) {
                PulsePlanListElem *ppRef = (PulsePlanListElem *)malloc(sizeof(PulsePlanListElem));
                memcpy(&(ppRef->pulsePlan),&pp,sizeof(PulsePlan));

                // Add the pulse plan to the list

                List_put(&pulsePlanList,(List_Elem *)ppRef);
            }

         }

        return true;

    } else {
        addErrorMessage(pulsePlanArray,"loadPulsePlanProperties","pulsePlan array is empty");
        return false;
    }

}

/**
 * Parses the JSON pulseParams property and stores the pulse information
 *
 *     @param pulseProps   Pulse properties JSON object
 *     @param pParams      Pointer to pulse parameter data structure
 *     @param resultsJson  Pointer to results JSON character string
 *
 *     @return Updated results JSON string
 */
bool loadPulseProperties(const json_t *pulseProps,PulseParams *pParams) {


    json_t const* pw = json_getProperty( pulseProps, "pulseWidth" );
    if ( !pw || JSON_INTEGER != json_getType( pw ) ) {
        addErrorMessage(pw,"loadPulseProperties","pulseWidth not an integer");
        return false;
    } else {
        pParams->width = (int)json_getInteger( pw );

        //PRI
        json_t const *pri = json_getProperty( pulseProps, "pri" );
        if ( !pri || JSON_INTEGER != json_getType( pri ) ) {
            addErrorMessage(pri,"loadPulseProperties","pri not an integer number");
            return false;
        } else {
            pParams->pri = json_getInteger( pri ) * 1000;

            //Phase delay
             json_t const *pd = json_getProperty( pulseProps, "phaseDelay" );
             if ( !pd || JSON_INTEGER != json_getType( pd ) ) {
                 addErrorMessage(pd,"loadPulseProperties","phaseDelay not an integer number");
                 return false;
             } else {
                 pParams->phaseDelay = json_getInteger( pd );
             }

        }

    }

    return true;

}


/**
 * Parses the JSON temperature alert property and stores the information
 *
 *     @param tempJson   Temperature properties JSON object
 */
void loadTempAlertProperties(const json_t *tempJson) {


    json_t const* ambient = json_getProperty( tempJson, "ambient" );
    if ( !ambient || JSON_INTEGER != json_getType( ambient ) ) {
        addErrorMessage(tempJson,"loadTempAlertProperties","Temp alert param ambient not an integer");
    } else {
        tempAlerts.ambient = (int)json_getInteger( ambient );
    }

    json_t const* pulser = json_getProperty( tempJson, "pulser" );
    if ( !pulser || JSON_INTEGER != json_getType( pulser ) ) {
        addErrorMessage(tempJson,"loadTempAlertProperties","Temp alert param pulser not an integer");
     } else {
        tempAlerts.pulser = (int)json_getInteger( pulser );
    }

    json_t const* powerSupply = json_getProperty( tempJson, "powerSupply" );
    if ( !powerSupply || JSON_INTEGER != json_getType( powerSupply ) ) {
        addErrorMessage(tempJson,"loadTempAlertProperties","Temp alert param powerSupply not an integer");
    } else {
        tempAlerts.powerSupply = (int)json_getInteger( powerSupply );
    }

}

/**
 * Parses the JSON alert thresholds property and stores the information
 *
 *     @param alertParams  Alert threshold properties JSON object
 *     @param thresholds   Pointer to alert thresholds
 *
 */
void loadAlertParams(const json_t *alertParams,AlertThresholds *thresholds) {

    json_t const* high = json_getProperty( alertParams, "high");
    if ( !high || JSON_INTEGER != json_getType( high ) ) {
        addErrorMessage(alertParams,"loadAlertParams","Alert param high not an integer");
     } else {
        thresholds->high = (int)json_getInteger( high );
    }

    json_t const* low = json_getProperty( alertParams, "low");
    if ( !low || JSON_INTEGER != json_getType( low ) ) {
        addErrorMessage(alertParams,"loadAlertParams","Alert param low not an integer");
    } else {
        thresholds->low = (int)json_getInteger( low );
    }
}

/**
 * Start the count down timer until the next pulse plan
 * will start
 */
void startPulsePlanTimer(int duration) {
  int *durationPtr = (int *)malloc(sizeof(int));
  *durationPtr = duration;
  startThread(&pulsePlanTimerThread,2,1,1024,(void *)durationPtr);
}

int getPulsePlanListSize(List_List *list) {
    int count = 0;

    PulsePlanListElem *pPlanElem = NULL;

    // Get the current plan list size
     for(pPlanElem = (PulsePlanListElem *)List_head(&pulsePlanList); pPlanElem != NULL;pPlanElem = (PulsePlanListElem *)List_next((List_Elem *)pPlanElem)) {
         count++;
     }

     return count;
}

PulsePlanListElem *findPulsePlanElem(PulsePlan *pulsePlan) {
    if ( pulsePlan == NULL ) {
        return NULL;
    }

    PulsePlanListElem *pPlanElem = NULL;

    // Get the current plan list size
    for(pPlanElem = (PulsePlanListElem *)List_head(&pulsePlanList); pPlanElem != NULL;pPlanElem = (PulsePlanListElem *)List_next((List_Elem *)pPlanElem)) {
        if ( pPlanElem->pulsePlan.id == pulsePlan->id) {
            break;
        }
    }

    return pPlanElem;

}

void *pulsePlanTimerThread(void *arg0) {

    int *durationPtr = (int *)arg0;
    int timerDuration = *durationPtr;
    timerRunning = true;
    timerCount = 0;



    while ( timerRunning && timerCount < timerDuration ) {
        timerCount++;
        sleep(1);
    }

    timerRunning = false;

    if ( timerCount == timerDuration ) {
        memcpy(&currentPulsePlan,&nextPulsePlan,sizeof(PulsePlan));
        processPWM(&(currentPulsePlan.pulseParams));
        int size = 0;

        PulsePlanListElem *nextPlanElem = NULL;
        PulsePlanListElem *currentPlanElem = NULL;

        size = getPulsePlanListSize(&pulsePlanList);

        if ( size == 1 ) {
            memcpy(&nextPulsePlan,&currentPulsePlan,sizeof(PulsePlan));
        } else if ( size > 1 ) {
            currentPlanElem = findPulsePlanElem(&currentPulsePlan);
            // We have more that one entry so get the next plan in the list
            nextPlanElem = (PulsePlanListElem *)List_next((List_Elem *)currentPlanElem);
            if ( nextPlanElem == NULL ) {
                // We are at the end of the list so get the first element in the list
                nextPlanElem = (PulsePlanListElem *)List_head(&pulsePlanList);
            }
            memcpy(&nextPulsePlan,&(nextPlanElem->pulsePlan),sizeof(PulsePlan));
            startPulsePlanTimer(currentPulsePlan.duration);
        }
    }

    free(durationPtr);
    return NULL;
}

void startPulsePlanMode() {

    PulsePlanListElem *pPlan = NULL;
    int count = 0;

    // Count the items in the plan list
    for(pPlan = (PulsePlanListElem *)List_tail(&pulsePlanList); pPlan != NULL;pPlan = (PulsePlanListElem *)List_prev((List_Elem *)pPlan)) {
        count++;
    }

    PulsePlanListElem *firstPlanElem = (PulsePlanListElem *)List_head(&pulsePlanList);
    if ( firstPlanElem != NULL) {
        // Get the current time

        PulsePlanListElem *nextPlanElem = NULL;
        PulsePlanListElem *currentPlanElem = NULL;

        if ( count == 1 ) {
            // We have only one plan so just run it
            currentPlanElem = firstPlanElem;
            nextPlanElem = currentPlanElem;
            processPWM(&((currentPlanElem->pulsePlan).pulseParams));
        } else {


            time_t t = time(NULL);
            struct tm *currentTime;
            currentTime = localtime(&t);

            int secsSinceSod = currentTime->tm_hour*3600 + currentTime->tm_min*60 + currentTime->tm_sec;

            // Check the pulse plan list and determine when the next pulse plan occurs


            for(pPlan = (PulsePlanListElem *)List_tail(&pulsePlanList); pPlan != NULL;pPlan = (PulsePlanListElem *)List_prev((List_Elem *)pPlan)) {
                currentPlanElem = pPlan;
                if ( pPlan->pulsePlan.startTime <= secsSinceSod ) {
                    break;
                }
            }

            if ( currentPlanElem == firstPlanElem && currentPlanElem->pulsePlan.startTime > secsSinceSod  ) {
                // All our plan start times are after our current time so set the current
                // plan to the last on the list and the next plan to the first.
                currentPlanElem = (PulsePlanListElem *)List_tail(&pulsePlanList);
            }

            // We have more that one entry so get the next plan in the list
            nextPlanElem = (PulsePlanListElem *)List_next((List_Elem *)currentPlanElem);
            if ( nextPlanElem == NULL ) {
                // We are at the end of the list so get the first element in the list
                nextPlanElem = (PulsePlanListElem *)List_head(&pulsePlanList);
            }

            int duration = currentPlanElem->pulsePlan.duration;
            // If the current time falls in between the current and next plan start times
            // adjust the nextPlan duration to the nextPlan start time less the current time of day
            if (currentPlanElem->pulsePlan.startTime < secsSinceSod && secsSinceSod < nextPlanElem->pulsePlan.startTime ) {

                duration = nextPlanElem->pulsePlan.startTime - secsSinceSod;
                // We have a current plan to run

            } else if ( (currentPlanElem->pulsePlan.startTime < secsSinceSod) && (currentPlanElem->pulsePlan.startTime >  nextPlanElem->pulsePlan.startTime) ) {
                // The current plan start time is later than the next plan so adjust the duration
                if (currentPlanElem->pulsePlan.startTime < secsSinceSod && secsSinceSod <= 86400 ) {
                    duration = 86400 - secsSinceSod + nextPlanElem->pulsePlan.startTime;
                } else if (secsSinceSod >= 0 && secsSinceSod < nextPlanElem->pulsePlan.startTime) {
                    duration = nextPlanElem->pulsePlan.startTime - secsSinceSod;
                }
            } else if ( currentPlanElem->pulsePlan.startTime > secsSinceSod && secsSinceSod < nextPlanElem->pulsePlan.startTime   ) {
                duration = nextPlanElem->pulsePlan.startTime - secsSinceSod;
            }

            // We have a current plan to run
            processPWM(&((currentPlanElem->pulsePlan).pulseParams));
            // Set the timer to start the next plan
            memcpy(&currentPulsePlan,&(currentPlanElem->pulsePlan),sizeof(PulsePlan));
            memcpy(&nextPulsePlan,&(nextPlanElem->pulsePlan),sizeof(PulsePlan));

            startPulsePlanTimer(duration);


        }

    } else {
        // This is just a fail safe.  Normally we should never get here
        // There are no pulse plans in the list so just run the default settings
        processPWM(&defaultPulseParams);
    }

}

void stopPulsePlanTimer() {
   timerRunning = false;
}

void stopPulsePlanMode() {

    stopPulsePlanTimer();

}
/**
 * Parses the JSON suspend operations thresholds property and stores the information
 *
 *     @param suspendOpsJson   Suspend ops threshold properties JSON object
 */
void loadSuspensOpsParams(const json_t *suspendOpsJson) {

    json_t const* temp = json_getProperty( suspendOpsJson, "temp");
    if ( !temp || JSON_INTEGER != json_getType( temp ) ) {
        addErrorMessage(suspendOpsJson,"loadSuspensOpsParams","Alert param temp not an integer");
    } else {
        soThresholds.tempThresh = (int)json_getInteger( temp );
    }

    json_t const* current = json_getProperty( suspendOpsJson, "current");
    if ( !current || JSON_INTEGER != json_getType( current ) ) {
        addErrorMessage(current,"loadAlertParams","Alert param current not an integer");
    } else {
         soThresholds.currentThresh = (int)json_getInteger( current );
    }

}


/**
 * Parses the setup command JSON and stores the retrieved data
 *
 *     @param commandObj    Setup command JSON object
 *     @param resultsJson   Results JSON string
 *
 *     @return Updated results JSON string
 */
char *processSetupCommand(const json_t *commandObj,char *resultsJson) {

    // If has defaults tag
    json_t const* pulseDefaults = json_getProperty( commandObj, "defaults" );
    if ( pulseDefaults && JSON_OBJ == json_getType( pulseDefaults ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: pulseDefaults setup config found");
        loadPulseProperties(pulseDefaults,&defaultPulseParams);
        pulseDefaultPropsSet = true;
    } else {
        addErrorMessage(pulseDefaults,"loadPulseProperties","pulseDefaults is not a valid JSON object");
    }

    // Pulse plan

    json_t const* pulsePlan = json_getProperty( commandObj, "pulsePlan" );
    if ( pulsePlan && JSON_ARRAY == json_getType( pulsePlan ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: pulsePlan setup config found");
        if ( loadPulsePlanProperties(pulsePlan) && !pulsePlanPropsSet ) {
            pulsePlanPropsSet = true;
        }
    } else {
        addErrorMessage(commandObj,"processSetupCommand","pulsePlan must be an array object");
    }

    // Manual mode config
    json_t const* manualMode = json_getProperty( commandObj, "manualMode" );
    if ( manualMode && JSON_OBJ == json_getType( manualMode ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: manualMode setup config found");
        if ( loadPulseProperties(manualMode,&manModePulseParams) && !pulseManModePropsSet ) {
            pulseManModePropsSet = true;
        }
    }

    // Temperature alert
    json_t const* tempAlert = json_getProperty( commandObj, "tempAlert" );
    if ( tempAlert && JSON_OBJ == json_getType( tempAlert ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: tempAlert setup config found");
        loadTempAlertProperties(tempAlert);
    }

    // Current alert

    json_t const* currentAlertJson = json_getProperty( commandObj, "currentAlert" );
    if ( currentAlertJson && JSON_OBJ == json_getType( currentAlertJson ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: currentAlert setup config found");
        loadAlertParams(currentAlertJson,&currentAlerts);
    }

    // Voltage alert

    json_t const* voltageAlertJson = json_getProperty( commandObj, "voltageAlert" );
    if ( voltageAlertJson && JSON_OBJ == json_getType( voltageAlertJson ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: voltageAlert setup config found");
        loadAlertParams(voltageAlertJson,&voltageAlerts);
    }

    // Power alert

    json_t const* powerAlertJson = json_getProperty( commandObj, "powerAlert" );
    if ( powerAlertJson && JSON_OBJ == json_getType( powerAlertJson ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: powerAlertJson setup config found");
        loadAlertParams(powerAlertJson,&powerAlerts);
    }

    // Suspend ops

    json_t const* suspendOps = json_getProperty( commandObj, "suspendOps" );
    if ( suspendOps && JSON_OBJ == json_getType( suspendOps ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: suspendOps setup config found");
        loadSuspensOpsParams(suspendOps);
    }

    json_t const* pulseModeObj = json_getProperty( commandObj, "pulseMode" );
    if ( pulseModeObj && JSON_TEXT == json_getType( pulseModeObj ) ) {
        Display_printf(display, 0, 0, "processSetupCommand: pulseMode setup config found");
        char const *pulseMode = json_getValue(pulseModeObj);
        if ( strcmp(pulseMode,"default") == 0 ) {

            if (strcmp(currentPulseMode,"pulsePlan") == 0 ) {
                stopPulsePlanMode();
            }

            strcpy(currentPulseMode,"default");
            if ( pulseDefaultPropsSet ) {
                processPWM(&defaultPulseParams);
            }

        } else if (strcmp(pulseMode,"pulsePlan") == 0 ) {

            strcpy(currentPulseMode,"pulsePlan");
            // If we have pulse plan properties loaded run them
            // otherwise fall back to default params
            if ( pulsePlanPropsSet ) {
                stopPulsePlanMode();
                startPulsePlanMode();
            } else {
                processPWM(&defaultPulseParams);
            }

        } else if (strcmp(pulseMode,"manual") == 0 ) {

            if (strcmp(currentPulseMode,"pulsePlan") == 0 ) {
                stopPulsePlanMode();
            }
            strcpy(currentPulseMode,"manual");
            // If we have manual mode properties loaded run them
            // otherwise fall back to default params
            if ( pulseManModePropsSet ) {
                processPWM(&manModePulseParams);
            } else {
                processPWM(&defaultPulseParams);
            }

        } else {
            addErrorMessage(commandObj,"processSetupCommand","Invalid pulseMode specified in command message");
        }
    } else {
        Display_printf(display, 0, 0, "processSetupCommand: pulseMode not provided");
    }


    return resultsJson;

}

/**
 * Parses the query command JSON and stores the retrieved data
 *
 *     @param commandObj    Query command JSON object
 *     @param resultsJson   Results JSON string
 *
 *     @return Updated results JSON string
 */
char *processQueryCommand(const json_t *commandObj,char *resultsJson) {
   // Get all current settings
   // Check pulse plan tag and get those settings as well if specified
    Display_printf(display,0,0,"processQueryCommnand: processing query command");

    resultsJson = json_str(resultsJson,"pulseMode",currentPulseMode);
    // Add default pulse parameters
    resultsJson = json_objOpen(resultsJson,"defaults");
    resultsJson = json_int(resultsJson,"pulseWidth",defaultPulseParams.width);
    resultsJson = json_int(resultsJson,"pri",defaultPulseParams.pri);
    resultsJson = json_int(resultsJson,"phaseDelay",defaultPulseParams.phaseDelay);
    resultsJson = json_objClose( resultsJson );

    // Add manual pulse parameters

    resultsJson = json_objOpen(resultsJson,"manualMode");
    resultsJson = json_int(resultsJson,"pulseWidth",manModePulseParams.width);
    resultsJson = json_int(resultsJson,"pri",manModePulseParams.pri);
    resultsJson = json_int(resultsJson,"phaseDelay",manModePulseParams.phaseDelay);
    resultsJson = json_objClose( resultsJson );

    //TODO:  Add pulse plan config, if pulsePlan id is specified and exists just list
    // params for that plan otherwise dump all the current plans if configured

    // Add temp alert thresholds
    resultsJson = json_objOpen(resultsJson,"tempAlert");
    resultsJson = json_int(resultsJson,"ambient",tempAlerts.ambient);
    resultsJson = json_int(resultsJson,"pulser",tempAlerts.pulser);
    resultsJson = json_int(resultsJson,"powerSupply",tempAlerts.powerSupply);
    resultsJson = json_objClose( resultsJson );
    // Add current alert thresholds
    resultsJson = json_objOpen(resultsJson,"currentAlert");
    resultsJson = json_int(resultsJson,"high",currentAlerts.high);
    resultsJson = json_int(resultsJson,"low",currentAlerts.low);
    resultsJson = json_objClose( resultsJson );
    // Add power alert thresholds
    resultsJson = json_objOpen(resultsJson,"powerAlert");
    resultsJson = json_int(resultsJson,"high",powerAlerts.high);
    resultsJson = json_int(resultsJson,"low",powerAlerts.low);
    resultsJson = json_objClose( resultsJson );
    // Add suspend ops thresholds
    resultsJson = json_objOpen(resultsJson,"suspendOps");
    resultsJson = json_int(resultsJson,"temp",soThresholds.tempThresh);
    resultsJson = json_int(resultsJson,"current",soThresholds.currentThresh);
    resultsJson = json_objClose( resultsJson );


    return resultsJson;
}

/**
 * Adds the panel measurements to the results json string
 *
 *      @param  resultsJson     Results JSON string
 *      @return Updated results JSON string
 */
char *addMeasurements(char *resultsJson) {

    getPanelMeasurements();
    resultsJson = json_double(resultsJson,"panelCurrent",panelMeasurements.panelCurrent);
    resultsJson = json_double(resultsJson,"panelVoltage",panelMeasurements.panelVoltage);
    resultsJson = json_double(resultsJson,"panelPower",panelMeasurements.panelPower);

    return resultsJson;
}

/**
 * Parses the JSON the measure command object and stores the retrieved data
 *
 *     @param commandObj    Measure command JSON object
 *     @param resultsJson   Command results JSON string
 *     @return Updated command results JSON string
 */
char *processMeasureCommand(const json_t *commandObj,char *resultsJson) {
   // Get measurement information
    Display_printf(display,0,0,"processMeasureCommand: processing measure command");

    resultsJson = addMeasurements(resultsJson);

    return resultsJson;

}

json_t jsonBuffer[DATA_SIZE];


/**
 * Processes the JSON command from the server
 *
 *    @param cmdJson        the command json string
 *    @param resultsJson    the command results json string
 *
 *    @return 0 if successful
 */
int processCommand(const char *cmdJson,char *resultsJson) {


    //Reset error message count
    errorMsgCnt = 0;
    // Clear out error messages buffer
    memset(errorMessages,'\0',sizeof(errorMessages));

    resultsJson = json_objOpen(resultsJson,NULL);

    // Create the results json data to send back to the server
    resultsJson = json_objOpen(resultsJson,"commandData");
    resultsJson = json_int( resultsJson, "code", MSG_CODE_CMD_RESPONSE );
    resultsJson = json_int( resultsJson, "format", 0 );
    resultsJson = json_str( resultsJson, "mac",deviceConfigParams.macAddress);
    resultsJson = json_str( resultsJson, "sensorId",deviceConfigParams.sensorId);
    resultsJson = json_ulong( resultsJson, "iotMessageId",deviceConfigParams.messageCnt);

    // Parse JSON command

    if ( cmdJson == NULL ) {
        Display_printf(display,0,0,"processCommand: null message");
        while(1);
    }

    // Retrieve the command from the JSON message
    int msgSize = sizeof(jsonBuffer) / sizeof(*jsonBuffer);

//    Display_printf(display,0,0,"processCommand: creating json string handler size = %d", sizeof(jsonBuffer) / sizeof(*jsonBuffer));
    json_t const* rootObj = json_create( (char *)cmdJson, jsonBuffer, msgSize );
    if ( !rootObj ) {
        Display_printf(display,0,0,"processCommand: Error creating json handler");
        while(1);
    }

    json_t const* command = json_getProperty( rootObj, "command" );
    if ( !command ) {
        Display_printf(display,0,0,"processCommand: no command property found in json");
        return 1;
    } else if  ( JSON_OBJ != json_getType( command ) ) {
        addErrorMessage(command,"processCommand","commmand property must be a JSON object");
    }


    // Check for command type
    json_t const* type = json_getProperty( command, "type" );
    if ( !type || JSON_TEXT != json_getType( type ) ) {
        addErrorMessage(type,"processCommand","No type specified in command message");
    }

    char const *typeStr = json_getPropertyValue(command,"type");
    if ( strcmp(typeStr,"setup") == 0 ) {
        // If setup recover the type of setup and associated params then configure
        resultsJson = processSetupCommand(command,resultsJson);

    } else if (strcmp(typeStr,"query") == 0 ) {
        // If query get the query params and pass back to server
        resultsJson = processQueryCommand(command,resultsJson);

    } else if (strcmp(typeStr,"measure") == 0 ) {
        // If measure get the requested info and pass back to server
        resultsJson = processMeasureCommand(command,resultsJson);
    } else {
        addErrorMessage(command,"processCommand","Invalid type specified in command message");
    }

    // Get the status interval

    json_t const* statusInterval = json_getProperty( command, "statusInterval" );
    if ( statusInterval && JSON_INTEGER == json_getType( statusInterval ) ) {
        deviceConfigParams.statusInterval = json_getInteger( statusInterval );
    } else {
        addErrorMessage(rootObj,"processCommand","command must have an integer statusInterval property");
    }

    resultsJson = json_int( resultsJson, "statusInterval", deviceConfigParams.statusInterval );
    resultsJson = json_str( resultsJson, "type", typeStr );

    int i = 0;
    if ( errorMsgCnt > 0 ) {
        resultsJson = json_str(resultsJson,"status","ERROR");
        resultsJson = json_arrOpen(resultsJson,"errors");
        for ( i = 0; i<errorMsgCnt; i++ ) {
            resultsJson = json_objOpen(resultsJson,NULL);
            resultsJson = json_str(resultsJson,"errorMsg",errorMessages[i]);
            resultsJson = json_objClose(resultsJson);
        }
        resultsJson = json_arrClose(resultsJson);
    } else {
        resultsJson = json_str(resultsJson,"status","OK");
    }

    resultsJson = json_objClose(resultsJson);
    resultsJson = json_objClose(resultsJson);
    resultsJson = json_end(resultsJson);

    return 0;

}


void *commandProcessor(void *arg) {

    char buffer[DATA_SIZE];
    char results[DATA_SIZE];
    mqd_t mq_in;
    mqd_t mq_out;
    int bytesRead;
    /* initialize the queue attributes */
    struct mq_attr attr;

    attr.mq_flags = 0;
    attr.mq_maxmsg = 2;
    attr.mq_msgsize = DATA_SIZE;
    attr.mq_curmsgs = 0;

    timerRunning = false;

    Display_printf(display, 0, 0, "commandProcessor: started thread");

    mq_in = mq_open(COMMAND_MSG_QUEUE,O_RDONLY );
    //mq_out = mq_open(COMMAND_RES_QUEUE,O_WRONLY);
    mq_out = mq_open(COMMAND_RES_QUEUE, O_CREAT | O_WRONLY , 0644, &attr);


    processPWM(&defaultPulseParams);

    startThread(&httpCommandHandler,4,1,THREADSTACKSIZE*2,NULL);

    while(runThreads) {

        memset(buffer,'\0',DATA_SIZE);

        // Read command from queue
        bytesRead = mq_receive(mq_in, buffer, DATA_SIZE, NULL);
        if(bytesRead >= 0) {
            Display_printf(display, 0, 0, "commandProcessor: read command from queue: %s",buffer);

            if ( processCommand(buffer,results) == 0 ) {
                // Push command results to queue
                Display_printf(display, 0, 0, "commandProcessor: command results: %s",results);
                mq_send(mq_out, results, strlen(results), 0);
            }
        }
    }

    Display_printf(display, 0, 0, "commandProcessor: Thread terminated");

    return NULL;
}


