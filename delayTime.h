/*
 * delayTime.h
 *
 *  Created on: Mar 22, 2019
 *      Author: LTran
 */

#ifndef DELAYTIME_H_
#define DELAYTIME_H_

void delayActivate(Timer_PeriodUnits delayUnit, uint32_t delayVal);
void setTime1DelayOn (uint32_t x);


#endif /* DELAYTIME_H_ */
