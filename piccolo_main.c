/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,

 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== main_tirtos.c ========
 */
#include <stdint.h>

/* POSIX Header files */
#include <pthread.h>

/* RTOS header files */
#include <ti/sysbios/BIOS.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>

/* Example/Board Header files */
#include "Board.h"

#include "piccolo.h"
#include "piccolo_main_thread.h"
#include "thread_utils.h"
#include "console.h"
#include "pwm.h"
#include "usb_tirtos.h"
#include "timer.h"
#include "wifi.h"



/*
 * The following function is only used in applications that use the NDK TCP/IP
 * stack
 */
#if defined(__IAR_SYSTEMS_ICC__)
__weak void NDK_hookInit(int32_t id) {
}

#elif defined(__GNUC__) && !defined(__ti__)
void __attribute__((weak)) NDK_hookInit(int32_t id) {
}

#else
#pragma WEAK (NDK_hookInit)
void NDK_hookInit(int32_t id) {
}

#endif


/*
 *  ======== main ========
 */
int main(void)
{

    /* Call board init functions */
    Board_initGeneral();

    // pthread_t startThread(void *(*threadFunc)(void *),int priority,int detached,int stackSize,void *arg) {
    startThread(&mainThread,1,1,THREADSTACKSIZE*8,NULL);

#ifdef UART_CONSOLE_ENABLE
   startThread(&consoleThread,1,1,THREADSTACKSIZE*8,NULL);
   // startThread(&consoleThread,3,1,THREADSTACKSIZE*8,NULL);
#endif

     startThread(&pwmThread,1,1,THREADSTACKSIZE*8,NULL); //priority 1
  //  startThread(&pwmThread,3,1,THREADSTACKSIZE*8,NULL);

     startThread(&timerThread,1,1,THREADSTACKSIZE*8,NULL); //priority 1

    usbThread();
   /// timerThread();

#if 0
    //this segment intended to add swi
    ///swi
    Swi_Params swiParams;

    Swi_Params_init(&swiParams);
     swiParams.arg0 = 1;
     swiParams.arg1 = 0;
     swiParams.priority = (2);//2;
     swiParams.trigger = 0;
  //   void wifiDetect(void)
   //  Swi_construct(&swi0Struct, (Swi_FuncPtr)swi0Fxn, &swiParams, NULL);
     Swi_construct(&swi0Struct, (Swi_FuncPtr)wifiDetect, &swiParams, NULL);
     swi0Handle = Swi_handle(&swi0Struct);




#endif
    BIOS_start();

    return (0);
}


