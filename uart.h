/*
 * uart.h
 *
 *  Created on: Mar 13, 2019
 *      Author: LTran
 */

#ifndef UART_H_
#define UART_H_

#include <ti/drivers/UART.h>
void uart_flush(UART_Handle uart);



#endif /* UART_H_ */
