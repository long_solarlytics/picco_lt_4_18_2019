/*
 * httpClient.h
 *
 *  Created on: Jan 12, 2019
 *      Author: tjc2c
 */

#ifndef HTTPCLIENT_H_
#define HTTPCLIENT_H_

#include <pthread.h>

void *httpStatusHandler(void *arg0);
void *httpCommandHandler(void *arg0);

bool networkConnected;


#endif /* HTTPCLIENT_H_ */
