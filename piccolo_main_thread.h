/*
 * piccolo_main_thread.h
 *
 *  Created on: Dec 21, 2018
 *      Author: tjc2c
 */

#ifndef PICCOLO_MAIN_THREAD_H_
#define PICCOLO_MAIN_THREAD_H_

void * mainThread(void *arg0);


#endif /* PICCOLO_MAIN_THREAD_H_ */
