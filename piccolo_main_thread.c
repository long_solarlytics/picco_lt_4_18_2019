/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* POSIX Header files */
#include <pthread.h>
#include <mqueue.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <ti/drivers/GPIO.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/PWM.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/Timer.h>

/* TI-Driver includes */
#include <ti/display/Display.h>

/* SlNetSock support */
#include <ti/net/slnetsock.h>
#include <ti/net/slnetif.h>

/* NDK support */
#include <ti/ndk/inc/socketndk.h>
#include <ti/ndk/inc/os/osif.h>

/* Application includes */
#include "Board.h"
#include "piccolo.h"
#include "httpClient.h"
#include "thread_utils.h"
#include "commandProcessor.h"

Display_Handle display;

extern void ti_ndk_config_Global_startupFxn();
extern void ti_simplelink_host_config_Global_startupFxn();
extern void * TCPSocket(void *args);



#if 1

/*
 *  ======== mainThread ========
 *  Thread that initializes the hardware and starts up the NDK and SimpleLink
 *  host stacks.
 */
void * mainThread(void *arg0)
{
#ifdef DISPLAY_ENABLE
    Display_init();
#endif
    GPIO_init();
    Timer_init();
    SPI_init();
 //   ADC_init();
 //   PWM_init();


    runThreads = true;
    networkConnected = false;
#ifdef DISPLAY_ENABLE
    display = Display_open(Display_Type_UART, NULL);
    if(display == NULL)
    {
        /* Failed to open display driver */
        while(1)
        {
            ;
        }
    }
#endif
    // Initialize wifi and ethernet modules


    int status;

    status = SlNetSock_init(0);
#ifdef DISPLAY_ENABLE
    if(status != 0)
    {
        Display_printf(display, 0, 0, "SlNetSock_init fail (%d)\n",
                       status);
    }
#endif
    status = SlNetIf_init(0);
#ifdef DISPLAY_ENABLE
    if(status != 0)
    {
        Display_printf(display, 0, 0, "SlNetIf_init fail (%d)\n",
                       status);
    }
#endif
    ti_simplelink_host_config_Global_startupFxn();
    ti_ndk_config_Global_startupFxn();

    startThread(&httpStatusHandler,3,1,THREADSTACKSIZE*2,NULL);

#if 0
    while(runThreads) {
    }
#endif
    return NULL;


}
#endif
