/*
 * uart.c
 *
 *  Created on: Mar 13, 2019
 *      Author: LTran
 */
#ifndef __MSP432E401Y__
#define __MSP432E401Y__
#endif

#include "uart.h"
#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTMSP432E4.h>
#include <MSP_EXP432E401Y.h>
////////////////#include "msp432e401y.h"
//#include "Board.h"
extern bool UARTCharsAvail(uint32_t ui32Base);

void uart_flush(UART_Handle uart)
{
    char data[1];

    UARTMSP432E4_HWAttrs const *hwAttrs = uart->hwAttrs;

  //  UARTMSP432E4_HWAttrs const *hwAttrs = handle->hwAttrs;
    uint32_t baseAddr = hwAttrs->baseAddr;
    bool b = UARTCharsAvail(baseAddr);
    while (UARTCharsAvail(baseAddr))

         {
             UART_readPolling(uart, data, 1);
         }
}
