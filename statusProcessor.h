/*
 * statusProcessor.h
 *
 *  Created on: Jan 3, 2019
 *      Author: tjc2c
 */

#ifndef STATUSPROCESSOR_H_
#define STATUSPROCESSOR_H_

/**
 * Status related data structure definition
 */
typedef struct statusMsg_ {
    int code;
    int format;
    char deviceId[17];
    char MAC[13];
    int ambientTemp;
    int unitTemp1;
    int unitTemp2;
    char sensorId[65];
    char siteId[24];
    char dateTime[24];  //YYYY-MM-DDTHH:MM:SS
    char timeOffset[7]; //+-HH:MM
    double panelVoltage;
    double panelCurrent;
    double panelPower;
    int pulseVoltage;
    int pulseWidth;
    double pri;
    double phaseDelay;
    int pulsePlan;
    bool alert;
    int messageId;
    int sleepTime;
} statusMsg_t;


void getRegistrationData(char *rInfo);
void getStatusData(char *sInfo);

#endif /* STATUSPROCESSOR_H_ */
