/*
 * timer.h
 *
 *  Created on: Mar 21, 2019
 *      Author: LTran
 */

#ifndef TIMER_H_
#define TIMER_H_
#include <stddef.h>
#include <ti/drivers/Timer.h>
void *timerThread(void *arg0);


#endif /* TIMER_H_ */
