/*
 * thread_utils.h
 *
 *  This module contains helper functions that wrap
 *  POSIX thread calls for setting up and running threads
 *
 *  Created on: Dec 21, 2018
 *      Author: tjc2c
 */

#ifndef THREAD_UTILS_H_
#define THREAD_UTILS_H_


#include <pthread.h>
/* Stack size in bytes */
#define THREADSTACKSIZE    2048

pthread_t startThread(void *(*threadFunc)(void *),int priority,int detached,int stackSize,void *arg);

#endif /* THREAD_UTILS_H_ */
