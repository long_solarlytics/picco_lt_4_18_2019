/*
 * delayTime.c
 *
 *  Created on: Mar 22, 2019
 *      Author: LTran
 */
#include "Board.h"
#include "timer.h"

extern void timerCallback1(Timer_Handle myHandle);
extern Timer_Handle timer1;

static uint32_t timer1DelayOn;

void setTime1DelayOn (uint32_t x)
{
    timer1DelayOn = x;
}

void delayActivate(Timer_PeriodUnits delayUnit, uint32_t delayVal)
{
  //   Timer_Handle timer1;
     Timer_Params params;

     params.period = delayVal;

     params.periodUnits = delayUnit;
     params.timerMode = Timer_ONESHOT_CALLBACK;
   //  params.timerMode = Timer_CONTINUOUS_CALLBACK;
     params.timerCallback = timerCallback1;

     timer1 = Timer_open(Board_TIMER1, &params);

     if (timer1 == NULL) {
           /* Failed to initialized timer */
           while (1) {}
     }

     if (Timer_start(timer1) == Timer_STATUS_ERROR) {
         /* Failed to start timer */
         while (1) {}
     }
     timer1DelayOn = 1;
     while (timer1DelayOn){};

     Timer_close(timer1);

}


