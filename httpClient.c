/*
 * httpClient.c
 *
 *  Created on: Jan 12, 2019
 *      Author: tjc2c
 */


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <mqueue.h>

#include <pthread.h>


#include <ti/net/http/httpclient.h>

#include "Board.h"
#include "network_if.h"
#include "piccolo.h"
#include "json/tiny-json.h"


#include "httpClient.h"
#include "thread_utils.h"

#include "commandProcessor.h"
#include "statusProcessor.h"
#include "startsntp.h"



#define HOSTNAME              "http://ec2-184-72-127-218.compute-1.amazonaws.com:5000"
//#define HOSTNAME              "http://ec2-184-72-127-218.compute-1.amazonaws.com:5001"

//#define HOSTNAME "http://184.72.127.218"
//#define HOSTNAME              "http://www.example.com"
#define STATUS_REGISTER_URI           "/registration"
#define STATUS_REQUEST_URI           "/status"
#define COMMAND_REQUEST_URI           "/response"
//#define REQUEST_URI           "/"
//#define USER_AGENT            "HTTPClient (ARM; TI)"
#define USER_AGENT            "application/json"

char statusData[DATA_SIZE];
char respData[DATA_SIZE];
char commandData[DATA_SIZE];

extern void fdOpenSession();
extern void fdCloseSession();
extern void *TaskSelf();


/**
 * Initializes global variables and structures used
 * by the application.
 */
void initializeConfigParameters() {
    strcpy(deviceConfigParams.siteId,"SITE XYZ");
//    strcpy(deviceConfigParams.sensorId,"20190308134438");
    deviceConfigParams.messageCnt = 1;
    deviceConfigParams.statusInterval = DEF_STATUS_SLEEP_TIME_SECS;

    defaultPulseParams.phaseDelay = 1000;
    defaultPulseParams.pri = 15000;
    defaultPulseParams.width = 1000;
    strcpy(currentPulseMode,"default");

    pulsePlanPropsSet = false;
    pulseManModePropsSet = false;
    pulseDefaultPropsSet = true;


    // Initialize timers for pulse modulation
////    Timer_initTimersForPulseMode();


    getUniqueId(deviceConfigParams.sensorId);

    Display_printf(display,0,0,"MCU unique id = %s",deviceConfigParams.sensorId);

    getDeviceId(deviceConfigParams.deviceId);

    Display_printf(display,0,0,"MCU device id = %s",deviceConfigParams.deviceId);

    getMacAddress(deviceConfigParams.macAddress);

    Display_printf(display,0,0,"MCU macAddress = %s",deviceConfigParams.macAddress);

}


/*
 *  ======== printError ========
 */
void printError(char *errString, int code)
{
    Display_printf(display, 0, 0, "Error! code = %d, desc = %s\n", code,
            errString);
}

/*
 *  ======== printError ========
 */
void printErrorAndWait(char *errString, int code)
{
    Display_printf(display, 0, 0, "Error! code = %d, desc = %s\n", code,
            errString);
    while(1);
}



/*
 *  ======== statusHandler ========
 *  Thread function to communicate H&S info to H&S server.
 */
void *httpStatusHandler(void *arg0) {
    struct mq_attr attr;
    mqd_t mq;

    bool moreDataFlag = false;
    int16_t ret = 0;
    int16_t len = 0;
    HTTPClient_Handle httpClientHandle;
    int16_t statusCode;
    static bool isRegistered = false;

    fdOpenSession(TaskSelf());

    Display_printf(display, 0, 0, "httpStatusHandler: Thread started");

    initializeConfigParameters();

    startSNTP();
    /* initialize the queue attributes */
    attr.mq_flags = 0;
    attr.mq_maxmsg = 2;
    attr.mq_msgsize = DATA_SIZE;
    attr.mq_curmsgs = 0;
    char contentType[32];

    Display_printf(display, 0, 0, "httpStatusHandler: creating command message queue");
    /* create the message queue */
    mq = mq_open(COMMAND_MSG_QUEUE, O_CREAT | O_RDONLY , 0644, &attr);

    startThread(&commandProcessor,6,1,THREADSTACKSIZE*4,NULL);

    httpClientHandle = HTTPClient_create(&statusCode,0);

    if (statusCode < 0) {
        printError("httpStatusHandler: creation of http client handle failed", ret);
        fdCloseSession(TaskSelf());
        while(1);
    }

    strcpy(contentType,USER_AGENT);

    startSNTP();

    while (runThreads) {

        if (networkIsConnected()) {
            //  Send initial HTTP GET for registration
            //  If GET request fails, wait and try again

            if (statusCode < 0) {
                printError("httpStatusHandler: creation of http client handle failed", ret);
            }

            ret = HTTPClient_setHeader(httpClientHandle, HTTPClient_HFIELD_REQ_CONTENT_TYPE, (char *)contentType,strlen(USER_AGENT), HTTPClient_HFIELD_PERSISTENT);
            if (ret < 0) {
                printError("httpStatusHandler: setting request header failed", ret);
            }

            ret = HTTPClient_connect(httpClientHandle, HOSTNAME, 0, 0);
            if (ret < 0) {
                printError("httpStatusHandler: connect failed", ret);
                sleep(CONNECT_SLEEP_TIME_SECS);
                continue;
            }

            memset(statusData,'\0',DATA_SIZE);
            if ( !isRegistered ) {
                getRegistrationData(statusData);
                Display_printf(display, 0, 0, "httpStatusHandler: Posting JSON to registration uri %s, length = %d'\n",statusData, strlen(statusData));

                ret = HTTPClient_sendRequest(httpClientHandle, HTTP_METHOD_POST,STATUS_REGISTER_URI, statusData, strlen(statusData), 0);
                if (ret < 0) {
                    printError("httpStatusHandler: send failed", ret);
                    HTTPClient_disconnect(httpClientHandle);
                    sleep(CONNECT_SLEEP_TIME_SECS);
                    continue;
                }
            } else {
                getStatusData(statusData);

                Display_printf(display, 0, 0, "httpStatusHandler: Posting JSON to ongoing status uri %s'\n",statusData);

                ret = HTTPClient_sendRequest(httpClientHandle, HTTP_METHOD_POST,STATUS_REQUEST_URI, statusData, strlen(statusData), 0);
                if (ret < 0) {
                    printError("httpStatusHandler: send failed", ret);
                    HTTPClient_disconnect(httpClientHandle);
                    sleep(CONNECT_SLEEP_TIME_SECS);
                    continue;
                }
            }

            if (ret != HTTP_SC_OK) {
                printError("httpStatusHandler: cannot get status", ret);
                HTTPClient_disconnect(httpClientHandle);
                sleep(CONNECT_SLEEP_TIME_SECS);
                continue;
            } else {
                if ( !isRegistered ) {
                    isRegistered = true;
                }
            }

            Display_printf(display, 0, 0, "httpStatusHandler: HTTP Response Status Code: %d\n", ret);

            len = 0;
            memset(respData,'\0',DATA_SIZE);
            do {
                ret = HTTPClient_readResponseBody(httpClientHandle, respData, DATA_SIZE,&moreDataFlag);
                if (ret < 0) {
                    printError("httpStatusHandler: response body processing failed", ret);
                    break;
                }
                len += ret;
            } while (moreDataFlag);

            Display_printf(display, 0, 0, "%s (msg size = %d bytes, strlen = %d )",respData,len,strlen(respData));

            // Push the incoming response to the message queue
            mq_send(mq,respData,strlen(respData),0);

            ret = HTTPClient_disconnect(httpClientHandle);
            if (ret < 0) {
                printError("httpStatusHandler: disconnect failed", ret);
            }

            sleep(deviceConfigParams.statusInterval);

        } else {
            sleep(CONNECT_SLEEP_TIME_SECS);
            isRegistered = false;
        }

    }

    HTTPClient_destroy(httpClientHandle);

    fdCloseSession(TaskSelf());

    return NULL;

}


/**
 * Thread that reads command responses from the message queue and sends them to the server
 */
void *httpCommandHandler(void *arg0) {
    mqd_t mq_in;
    //mqd_t mq_out;
    bool moreDataFlag = false;
    int16_t ret = 0;
    int16_t len = 0;
    HTTPClient_Handle httpClientHandle;
    int16_t statusCode;

    fdOpenSession(TaskSelf());

    Display_printf(display, 0, 0, "httpCommandHandler: start");
    mq_in = mq_open(COMMAND_RES_QUEUE,  O_RDONLY );

    Display_printf(display, 0, 0, "httpCommandHandler: creating command results queue");
    /* create the message queue */

    httpClientHandle = HTTPClient_create(&statusCode,NULL);

    if (statusCode < 0) {
        printError("httpCommandHandler: creation of http client handle failed", ret);
        fdCloseSession(TaskSelf());
        while(1);
    }

    if (networkIsConnected()) {

            while ( runThreads && networkIsConnected() ) {
                //Display_printf(display, 0, 0, "httpCommandHandler: Doing some stuff...\n");
                // Read the command result data from the queue

                mq_receive(mq_in, commandData, DATA_SIZE, NULL);

                // POST the data to the server
                Display_printf(display, 0, 0, "httpCommandHandler: Sending a HTTP POST request to '%s'\n",
                        HOSTNAME);


                ret = HTTPClient_setHeader(httpClientHandle, HTTPClient_HFIELD_REQ_CONTENT_TYPE, USER_AGENT,strlen(USER_AGENT), HTTPClient_HFIELD_PERSISTENT);
                if (ret < 0) {
                    printError("httpCommandHandler: setting request header failed", ret);
                }

                ret = HTTPClient_connect(httpClientHandle, HOSTNAME, 0, 0);
                if (ret < 0) {
                    printError("httpCommandHandler: connect failed", ret);
                }

                ret = HTTPClient_sendRequest(httpClientHandle, HTTP_METHOD_POST,COMMAND_REQUEST_URI, commandData, strlen(commandData), 0);
                if (ret < 0) {
                    printError("httpCommandHandler: send failed", ret);
                }

                if (ret != HTTP_SC_OK) {
                    printError("httpCommandHandler: cannot send command data", ret);
                }

                Display_printf(display, 0, 0, "httpCommandHandler: HTTP Response Status Code: %d\n", ret);

                len = 0;
                memset(respData,'\0',DATA_SIZE);
                do {
                    ret = HTTPClient_readResponseBody(httpClientHandle, respData, DATA_SIZE,&moreDataFlag);
                    if (ret < 0) {
                        printError("httpCommandHandler: response body processing failed", ret);
                        break;
                    }

                    Display_printf(display, 0, 0, "%d %s",ret ,respData);
                    len += ret;
                } while (moreDataFlag);

                Display_printf(display, 0, 0, "httpCommandHandler: Received %d bytes of payload\n", len);

                //If we got an ACK from the server, check if there is a command.
                //If there is a command, recover it from the json and push to the command queue
                //mq_send(mq_out,respData,strlen(respData),0);

                ret = HTTPClient_disconnect(httpClientHandle);
                if (ret < 0) {
                    printError("httpCommandHandler: disconnect failed", ret);
                }
            }
    }

    HTTPClient_destroy(httpClientHandle);

    Display_printf(display, 0, 0, "httpCommandHandler: Thread terminated");

    fdCloseSession(TaskSelf());

    return NULL;
}
