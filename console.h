/*
 * console.h
 *
 *  Created on: Feb 26, 2019
 *      Author: LTran
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_


#if 0
/* Console display strings */
const char consoleDisplay[]   = "\fConsole (h for help)\r\n";
const char helpPrompt[]       = "Valid Commands\r\n"                  \
                                "--------------\r\n"                  \
                                "h: help\r\n"                         \
                                "q: quit and shutdown UART\r\n"       \
                                "c: clear the screen\r\n"             \
                                "t: display current temperature\r\n";
const char byeDisplay[]       = "Bye! Hit button1 to start UART again\r\n";
const char tempStartDisplay[] = "Current temp = ";
const char tempMidDisplay[]   = "C (";
const char tempEndDisplay[]   = "F)\r\n";
const char cleanDisplay[]     = "\f";
const char userPrompt[]       = "> ";
const char readErrDisplay[]   = "Problem read UART.\r\n";

const char pulserCmd[]   = "p ";
const char pulserPeriod[]   = "p ";

const char command[]   = "command = ";
const char returnNl[] = "\r\n" ;
#endif

void *consoleThread(void *arg0);
///void simpleConsoleUsb(void);

void *simpleConsoleUsb(void *);


#endif /* CONSOLE_H_ */
