/*
 * thread_utils.c
 *
 *  Created on: Dec 21, 2018
 *      Author: tjc2c
 */


#include "thread_utils.h"


/**
 * Starts a posix thread
 *   @param threadFunct  Posix thread function reference
 *   @param priority     Thread priority value
 *   @param detached     set to 1 if the thread is to run in detached mode
 *   @param stackSize    Stack size in bytes to allocate to this thread
 *   @param arg          Pointer to argument data passed to thread
 *
 *   @return Thread reference id
 */
pthread_t startThread(void *(*threadFunc)(void *),int priority,int detached,int stackSize,void *arg) {
    pthread_t thread;
    pthread_attr_t pAttrs;
    struct sched_param priParam;
    int retc;
    int detachState;

    /* Set priority and stack size attributes */
    pthread_attr_init(&pAttrs);
    priParam.sched_priority = priority;

    if ( detached ) {

        detachState = PTHREAD_CREATE_DETACHED;
        retc = pthread_attr_setdetachstate(&pAttrs, detachState);
        if(retc != 0)
        {

            /* pthread_attr_setdetachstate() failed */
            while(1)
            {
                ;
            }
        }
    }

    pthread_attr_setschedparam(&pAttrs, &priParam);

    retc |= pthread_attr_setstacksize(&pAttrs, stackSize);
    if(retc != 0)
    {
        /* pthread_attr_setstacksize() failed */
         while(1)
        {
            ;
        }
    }

    retc = pthread_create(&thread, &pAttrs, threadFunc, arg);
    if(retc != 0)
    {
        /* pthread_create() failed */
        while(1)
        {
            ;
        }
    }

    pthread_attr_destroy(&pAttrs);

    return thread;

}

